#!/bin/bash

export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
export PATH=/usr/local/bin:$PATH

rsync -avz /build/etc/xmq /etc/

RUN_TESTS="true"
if [ "$1" = "--no-tests" ]; then
    RUN_TESTS="false"
    shift
fi

# Build scroipt for building either SPTK or XMQ packages in Docker environment

for PACKAGE in $@; do

echo ═════════════════════════════ $PACKAGE ═══════════════════════════════

if [ ! "$PACKAGE" = "SPTK" ] && [ ! "$PACKAGE" = "XMQ" ]; then
    echo "Please provide package name, SPTK or XMQ"
    exit 1
fi

OS_NAME=$(grep -E "^ID=" /etc/os-release | sed -re 's/^ID=//; s/"//g')
OS_VERSION=$(grep -E "^VERSION_ID=" /etc/os-release | sed -re 's/^VERSION_ID=//; s/"//g')
OS_CODENAME=$(grep -E '^VERSION_CODENAME=' /etc/os-release | sed -re 's/^.*=(\w+)?.*$/\1/')  #'
PLATFORM=$(grep -E '^PLATFORM_ID=' /etc/os-release | sed -re 's/^.*:(\w+).*$/\1/')  #'

OS_FULLNAME=$OS_NAME
if [ "$OS_NAME" = "ol" ]; then
    OS_FULLNAME="oraclelinux"
fi

if [ "$OS_CODENAME" = "" ]; then
    OS_CODENAME=$OS_VERSION
fi

VERSION=$(head -1 /build/scripts/${PACKAGE}_VERSION)
RELEASE="1"
PACKAGE_NAME="$PACKAGE-$VERSION"

DOWNLOAD_DIRNAME=$OS_NAME-$OS_CODENAME
OS_TYPE="$OS_NAME-$OS_VERSION"
case $OS_NAME in
    ubuntu)
        OS_TYPE="ubuntu-$OS_VERSION"
        ;;

    ol)
        OS_TYPE="$PLATFORM"
        DOWNLOAD_DIRNAME="oraclelinux9"
        ;;

    fedora)
        OS_TYPE="fc$OS_VERSION"
        ;;
esac

echo OS_NAME:   $OS_NAME
echo PLATFORM:  $PLATFORM
echo PACKAGE:   $PACKAGE_NAME
echo ──────────────────────────────────────────────────────────────────
cd /build/$PACKAGE_NAME || exit

CWD=`pwd`
./distclean.sh

if [ $PACKAGE = "SPTK" ]; then
    TAR_DIR="/build/output/${PACKAGE}-${VERSION}/tar"
    mkdir -p "${TAR_DIR}"
    src_name="${TAR_DIR}/${PACKAGE_NAME}"
    echo "Base tar name: ${src_name}" > make_src_archives.log
    [ ! -f ${src_name}.txz ] && tar Jcf ${src_name}.txz --exclude-from=exclude_from_tarball.lst * >> make_src_archives.log
    [ ! -f ${src_name}.zip ] && zip -r ${src_name}.zip * --exclude '@exclude_from_tarball.lst' >> make_src_archives.log
fi

if [ $PACKAGE = "SPTK" ]; then
    BUILD_OPTIONS="-DUSE_GTEST=ON -DBUILD_EXAMPLES=OFF -DCMAKE_INSTALL_PREFIX=/usr/local"
else
    BUILD_OPTIONS="-DCMAKE_INSTALL_PREFIX=/usr/local"
fi

sh ./distclean.sh
cmake . $BUILD_OPTIONS && make -j6 package || exit 1

echo ──────────────────────────────────────────────────────────────────
BUILD_OUTPUT_DIR=/build/output/$PACKAGE-$VERSION
sh ./install_local_packages.sh
mkdir -p $BUILD_OUTPUT_DIR && chmod 777 $BUILD_OUTPUT_DIR || exit 1
echo ──────────────────────────────────────────────────────────────────

OUTPUT_DIR=$BUILD_OUTPUT_DIR/$DOWNLOAD_DIRNAME
mkdir -p $OUTPUT_DIR || exit 1
for fname in $(ls *.rpm *.deb)
do
    if [ $PACKAGE = "SPTK" ]; then
        name=$(echo $fname | sed -re 's/^SPTK/sptk/;s/-Linux//')
        lcPACKAGE="sptk"
    else
        name=$(echo $fname | sed -re 's/^XMQ/xmq/;s/-Linux//')
        lcPACKAGE="xmq"
    fi
    mv $fname $OUTPUT_DIR/$name
done

sh ./distclean.sh

echo ──────────────────────────────────────────────────────────────────

if [ $RUN_TESTS = "true" ]; then
    export LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64:/opt/oracle/instantclient_18_3:${LD_LIBRARY_PATH}
    grep "10.1.1.242" /etc/hosts
    if [ $? == 1 ]; then
        echo "10.1.1.242  theater oracledb dbhost_oracle dbhost_mssql dbhost_pg dbhost_mysql smtp_host" >> /etc/hosts
    fi

    export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
    cd $CWD/test && ${lcPACKAGE}_unit_tests 2>&1 > /build/logs/${lcPACKAGE}_unit_tests.$OS_TYPE.log
    RC=$?

    if [ $RC != 0 ]; then
        echo "/build/logs/${lcPACKAGE}_unit_tests.$OS_TYPE.log" > /build/logs/${lcPACKAGE}_failed.log
    else
        rm /build/logs/${lcPACKAGE}_failed.log
    fi
fi

cd $CWD
sh ./distclean.sh
chown -R alexeyp *

done

exit $RC
