/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexey overridep@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#pragma once

#include <FL/Fl_Widget.H>
#include <sptk5/xdoc/Node.h>
#include <string>

namespace sptk {

/**
 * @addtogroup gui GUI Classes
 * @{
 */

/**
 * Possible values for the widget layout aligns
 */
enum class CLayoutAlign : uint8_t
{
    NONE,   /// Do not use allignment
    LEFT,   /// Align to the left
    RIGHT,  /// Align to the right
    TOP,    /// Align to the top
    BOTTOM, /// Align to the bottom
    CLIENT  /// Use the whole available space
};

/**
 * Layout load and save mode
 */
enum class CLayoutXMLmode : uint8_t
{
    /**
     * Load and save only controls data
     */
    DATA = 1,

    /**
     * Load and save only controls layout
     */
    LAYOUT = 2,

    /**
     * Load and save controls data and layout
     */
    LAYOUTDATA = 3
};

/**
 * Base layout manager client.
 *
 * Allows CLayoutManager objects to move and resize
 * the CLayoutClient objects in accordancy with they preferredSize() and
 * layout alignment.
 */
class SP_EXPORT CLayoutClient
{
    friend class CLayoutManager;

    /**
     * The preferred layout size
     *
     * It makes different sense depending on the widget layout align.
     * For layout align CLayoutAlign::TOP or CLayoutAlign::BOTTOM it is a recomended height of the widget.
     * For layout align CLayoutAlign::LEFT or CLayoutAlign::RIGHT it is a recomended width of the widget.
     * For layout align CLayoutAlign::NONE or CLayoutAlign::CLIENT it is ignored.
     * If the widget isn't in CLayoutManager group - it is ignored.
     * If it doesn't contradict with preferred widget size it will define the final size of the widget.
     */
    int m_layoutSize;

    /**
     * The width as a result of the last call of preferredSize()
     */
    int m_lastPreferredW {0};

    /**
     * The width as a result of the last call of preferredSize()
     */
    int m_lastPreferredH {0};

protected:
    /**
     * @return last preferred height
     */
    int lastPreferredH() const;

    /**
     * @return last preferred width
     */
    int lastPreferredW() const;

    /**
     * Set last preferred height
     * @param height            Height
     */
    void lastPreferredH(int height);

    /**
     * Set last preferred width
     * @param width             Width
     */
    void lastPreferredW(int width);

    /**
     * Widget to manage
     */
    Fl_Widget* m_widget;

    /**
     * Widget name (widget id)
     */
    String m_name;

    /**
     * Widget caption storage
     */
    String m_label;

    /**
     * The layout align for the widget in CLayoutManager group
     */
    CLayoutAlign m_layoutAlign;

public:
    /**
     * Constructor
     * @param widget Fl_Widget*, widget to control
     * @param layoutSize int, the size of the widget in layout. See m_layoutSize for more information.
     * @param ca CLayoutAlign, widget align in layout
     */
    CLayoutClient(Fl_Widget* widget, int layoutSize, CLayoutAlign ca);

    /**
     * Destructor
     */
    virtual ~CLayoutClient()
    {
    }

    /**
     * Returns widget's layout align
     * @see CLayoutAlign
     */
    CLayoutAlign layoutAlign() const
    {
        return m_layoutAlign;
    }

    /**
     * Sets widget's layout align
     * @see CLayoutAlign
     */
    void layoutAlign(CLayoutAlign al)
    {
        m_layoutAlign = al;
    }

    /**
     * Returns widget's layout size
     */
    int layoutSize() const
    {
        return m_layoutSize;
    }

    /**
     * Sets widget's layout size
     */
    void layoutSize(int ls)
    {
        m_layoutSize = ls;
    }

    /**
     * Sets label, makes an internal copy of the string
     * @param l                 New label
     */
    virtual void label(const String& l)
    {
        m_label = l;
        m_widget->label(m_label.c_str());
    }

    /**
     * Returns the current label
     */
    virtual const String& label() const
    {
        return m_label;
    }

    /**
     * Sets the new widget name
     *
     * @param aname const char*, new widget name
     */
    void name(const char* aname)
    {
        m_name = aname;
    }

    /**
     * Sets the new widget name
     *
     * @param aname             New widget name
     */
    void name(const String& aname)
    {
        m_name = aname;
    }

    /**
     * Returns the current name
     */
    const String& name() const
    {
        return m_name;
    }

    /**
     * Computes widget's preferred size.
     *
     * Should be overriten in derived widget if it has any ideas about it's size limits.
     * Widget may want to change none, one, or both preferred width and height suggested by the
     * CLayoutManager.
     * @param w int&, input/output widget preferred width
     * @param h int&, input/output widget preferred height
     * @returns true if the size is stable (doesn't depend on input sizes)
     */
    virtual bool preferredSize(int& w, int& h)
    {
        return false;
    }

    /**
     * Computes widget's preferred size, and stores size values internally as cache
     *
     * Used internally by CLayoutManager.
     * @param w int&, input/output widget preferred width
     * @param h int&, input/output widget preferred height
     */
    virtual bool computeSize(int& w, int& h)
    {
        bool rc = preferredSize(w, h);
        m_lastPreferredW = w;
        m_lastPreferredH = h;
        return rc;
    }

    /**
     * Returns widget class name (internal SPTK RTTI).
     */
    virtual String className() const
    {
        return "Undefined";
    }

    /**
     * Returns widget handled by that object
     */
    Fl_Widget* widget() const
    {
        return m_widget;
    }

    /**
     * Loads layout client information from XML node
     *
     * Layout information may also include widget size and position,
     * as well as visible() and active() states
     * @param node              the XML node
     * @param xmlMode           the mode defining how the layout and/or data should be stored
     */
    virtual void load(const xdoc::SNode& node, CLayoutXMLmode xmlMode);

    /**
     * Loads layout client information from XML node
     *
     * Layout information may also include widget size and position,
     * as well as visible() and active() states
     * @param node              the XML node
     */
    virtual void load(const std::shared_ptr<xdoc::Node>& node)
    {
        load(node, CLayoutXMLmode::LAYOUT);
    }

    /**
     * Saves layout client information from XML node
     *
     * Layout information may also include widget size and position,
     * as well as visible() and active() states
     * @param node              the XML node
     * @param xmlMode           the mode defining how the layout and/or data should be stored
     */
    virtual void save(const std::shared_ptr<xdoc::Node>& node, CLayoutXMLmode xmlMode) const;
};
/**
 * @}
 */
} // namespace sptk
