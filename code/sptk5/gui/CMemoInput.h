/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#pragma once

#include <sptk5/sptk.h>

#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Input_.H>
#include <sptk5/DateTime.h>
#include <sptk5/db/PoolDatabaseConnection.h>
#include <sptk5/db/Query.h>
#include <sptk5/gui/CControl.h>
#include <sptk5/gui/CInput.h>

#include <sptk5/gui/CBox.h>

namespace sptk {

/**
 * @addtogroup gui GUI Classes
 * @{
 */

class CPopupWindow;

class CPopupCalendar;

class CDateControl;

class CToggleTree;

/**
 * Simple text editor
 *
 * Multiple line input box
 */
class SP_EXPORT CMemoInput
    : public CInput
{
    using inherited = class CInput;

    /**
     * Constructor initializer
     */
    void ctor_init();

public:
    /**
     * Constructor in SPTK style
     * @param label const char *, label
     * @param layoutSize int, widget align in layout
     * @param layoutAlign CLayoutAlign, widget align in layout
     */
    CMemoInput(const char* label = 0, int layoutSize = 10, CLayoutAlign layoutAlign = CLayoutAlign::TOP);

#ifdef __COMPATIBILITY_MODE__
    /**
     * Constructor in FLTK style
     * @param x int, x-position
     * @param y int, y-position
     * @param w int, width
     * @param h int, height
     * @param label, const char * label
     */
    CMemoInput(int x, int y, int w, int h, const char* label = 0);
#endif

    /**
     * Returns the control kind, SPTK-style RTTI
     * @see CControlKind for more information
     */
    CControlKind kind() const override
    {
        return CControlKind::MEMO;
    }

    /**
     * Returns the control class name, SPTK-style RTTI
     */
    String className() const override
    {
        return "memo";
    }

    /**
     * Universal data connection, returns data from control
     */
    Variant data() const override;

    /**
     * Universal data connection, sets data from control
     */
    void data(const Variant& v) override;

    /**
     * Returns the input text font type
     */
    Fl_Font textFont() const override;

    /**
     * Sets the input text font type
     */
    void textFont(Fl_Font f) override;

    /**
     * Returns the input text font size
     */
    uchar textSize() const override;

    /**
     * Sets the input text font size
     */
    void textSize(uchar s) override;

    /**
     * Saves data to query
     */
    void save(Query*) override;

    /**
     * Computes the optimal widget size
     * @param w int&, input - width offered by the program, output - width required by widget
     * @param h int&, input - height offered by the program, output - height required by widget
     * @returns true if the size is stable (doesn't depend on input sizes)
     */
    bool preferredSize(int& w, int& h) override;

    /**
     * Creates a widget based on the XML node information
     */
    static CLayoutClient* creator(const xdoc::SNode& node);
};
/**
 * @}
 */
} // namespace sptk
