/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#pragma once

#include "LogPriority.h"
#include <sptk5/DateTime.h>
#include <sptk5/sptk.h>

#include <mutex>

namespace sptk {

class LogEngine;

/**
 * @addtogroup log Log Classes
 * @{
 */

/**
 * @brief A log that sends all the log messages into another log.
 *
 * The destination log is locked for a message adding period.
 * Multiple Logger objects may send messages from different threads
 * into the same destination log.
 * The log options defining message format and min priority are used
 * from destination log.
 * @see BaseLog for more information about basic log abilities.
 */
class SP_EXPORT Logger
{
public:
    /**
     * @brief Log message
     */
    struct Message
    {
        DateTime    timestamp {"now"}; ///< Message timestamp
        LogPriority priority;          ///< Message priority
        String      message;           ///< Message text

        /**
         * @brief Constructor
         * @param priority       Message priority
         * @param message        Message text
         */
        Message(LogPriority priority, String message);
    };

    using UMessage = std::unique_ptr<Message>;

    /**
     * @brief Constructor
     * @param destination       Destination logger
     * @param prefix            Optional log message prefix
     */
    explicit Logger(LogEngine& destination, std::string_view prefix = "");

    /**
     * @brief Returns log engine (destination logger)
     */
    LogEngine& destination() const
    {
        const std::lock_guard lock(m_mutex);
        return m_destination;
    }

    /**
     * @brief Log message with any priority
     * @param priority          Message priority
     * @param message           Message text
     */
    void log(LogPriority priority, const String& message) const;

    /**
     * @brief Log message with debug priority
     * @param message           Message text
     */
    void debug(const String& message) const;

    /**
     * @brief Log message with info priority
     * @param message           Message text
     */
    void info(const String& message) const;

    /**
     * @brief Log message with notice priority
     * @param message           Message text
     */
    void notice(const String& message) const;

    /**
     * @brief Log message with warning priority
     * @param message           Message text
     */
    void warning(const String& message) const;

    /**
     * @brief Log message with error priority
     * @param message           Message text
     */
    void error(const String& message) const;

    /**
     * @brief Log message with critical priority
     * @param message           Message text
     */
    void critical(const String& message) const;

    /**
     * @brief Set log message prefix
     * @param prefix            Message prefix
     */
    void prefix(std::string_view prefix)
    {
        const std::lock_guard lock(m_mutex);
        m_prefix.assign(prefix.data(), prefix.size());
    }

    /**
     * @brief Get log message prefix
     */
    std::string_view prefix() const
    {
        const std::lock_guard lock(m_mutex);
        return m_prefix;
    }

    /**
     * @brief Test if a log priority is enabled
     * @param logPriority       Log priority
     * @return true if the priority is enabled
     */
    bool has(LogPriority logPriority) const;

private:
    mutable std::mutex m_mutex;       ///< Mutex that protects access to member variables
    LogEngine&         m_destination; ///< The actual log to store messages to (destination log)
    std::string        m_prefix;      ///< Log message prefix
};

using SLogger = std::shared_ptr<Logger>;

/**
 * @}
 */
} // namespace sptk
