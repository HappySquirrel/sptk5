/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#pragma once

#include <sptk5/Buffer.h>
#include <sptk5/CaseInsensitiveCompare.h>

#include <map>
#include <string>

namespace sptk {

/**
 * @addtogroup utility Utility Classes
 * @{
 */

/**
 * HTTP fields are implemented as case-insensitive map
 */
using StringHttpFieldMap = std::map<sptk::String, sptk::String, CaseInsensitiveCompare>;

class SP_EXPORT Url
{
public:
    /**
     * Encodes a string into HTML parameters
     */
    static String encode(const String& str);

    /**
     * Decodes a string from HTML parameters
     */
    static sptk::String decode(const String& str);
};

/**
 * HTTP params map
 *
 * Designed to hold HTTP parametrs in
 * CHttpConnect and CCgiApplication. It is, basically, a string-to-string
 * map with an addition of encode and decode functions for HTTP Mime.
 */
class SP_EXPORT HttpParams : public StringHttpFieldMap
{
public:
    /**
     * Default constructor.
     */
    HttpParams() = default;

    /**
     * Copy constructor.
     */
    HttpParams(const HttpParams& other) = default;

    /**
     * Initialization constructor.
     */
    HttpParams(std::initializer_list<std::pair<String, String>> lst);

    /**
     * Encodes HTTP parameters for sending to the server.
     * @param result            Output - encoded parameters string (if any) as the buffer.
     */
    void encode(Buffer& result) const;

    /**
     * Decodes HTTP parameters that came from the server as a string into parameters map.
     * @param buffer       Parameters string from HTTP server
     * @param lowerCaseNames    True if you want to lower-case the parameter names
     */
    void decode(const Buffer& buffer, bool lowerCaseNames = false);

    /**
     * Returns parameter value, or empty string if not found
     * @param paramName         Parameter name
     * @return parameter value
     */
    [[nodiscard]] String get(const String& paramName) const;

    /**
     * Returns true if parameter exists
     * @param paramName         Parameter name
     * @return true if parameter exists
     */
    [[nodiscard]] bool has(const String& paramName) const;
};
/**
 * @}
 */
} // namespace sptk
