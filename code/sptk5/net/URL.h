/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#pragma once

#include <sptk5/net/HttpParams.h>

namespace sptk {

class SP_EXPORT URL
{
public:
    explicit URL(const String& url);
    URL(const URL& other) = default;

    HttpParams& params()
    {
        return m_params;
    }

    [[nodiscard]] const HttpParams& params() const
    {
        return m_params;
    }

    [[nodiscard]] String protocol() const
    {
        return m_protocol;
    }

    [[nodiscard]] String username() const
    {
        return m_username;
    }

    [[nodiscard]] String password() const
    {
        return m_password;
    }

    [[nodiscard]] String hostAndPort() const
    {
        return m_hostAndPort;
    }

    [[nodiscard]] String path() const
    {
        return m_path;
    }

    [[nodiscard]] String location() const;

    [[nodiscard]] String toString() const;

    void path(const String& path)
    {
        m_path = path;
    }

private:
    String     m_protocol;
    String     m_username;
    String     m_password;
    String     m_hostAndPort;
    String     m_path;
    HttpParams m_params;
};

} // namespace sptk
