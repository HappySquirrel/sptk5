/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#pragma once

#ifdef _WIN32
#include <winsock2.h>

#include <windows.h>
#endif

#include <sptk5/Strings.h>
#include <sptk5/sptk.h>

namespace sptk {

/**
 * @addtogroup utility Utility Classes
 * @{
 */

/**
 * @brief Unique instance object
 *
 * Tries to create a mutex object and check if it's unique for the
 * program with the instance name. If one instance per user is desired
 * simply incorporate the user name into the instance.
 */
class SP_EXPORT UniqueInstance
{
public:
    /**
     * Constructor
     * @param instanceName      Instance name
     */
    explicit UniqueInstance(String instanceName);
    ~UniqueInstance();

    UniqueInstance(const UniqueInstance&) = delete;
    UniqueInstance& operator=(const UniqueInstance&) = delete;

#ifndef _WIN32
    /**
     * Return lock file name
     * @return lock file name
     */
    const String& lockFileName() const;
#endif // _WIN32

    /**
     * Reports true if the instance is unique
     */
    bool isUnique() const;

private:
    String m_instanceName;      ///< Instance name
    bool m_lockCreated {false}; ///< Lock is created
#ifdef _WIN32
    HANDLE m_mutex; ///< The named mutex object
#else
    String m_fileName;    ///< The lock file name
    int read_pid() const; ///< Gets the process ID
    int write_pid();      ///< Writes the process ID into the lock file
#endif

    void cleanup(); ///< Cleanup allocated resources
};
/**
 * @}
 */
} // namespace sptk
