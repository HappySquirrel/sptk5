/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                        SIMPLY POWERFUL TOOLKIT (SPTK)                        ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#pragma once

#include <occi.h>

#include <cstdio>
#include <list>
#include <string>

#include "DatabaseField.h"
#include <sptk5/FieldList.h>
#include <sptk5/db/DatabaseStatement.h>

namespace sptk {

class OracleConnection;

/**
 * Oracle statement
 */
class OracleStatement
    : public DatabaseStatement<OracleConnection, oracle::occi::Statement>
{
public:
    using Connection = oracle::occi::Connection; ///< Oracle connection type
    using Statement = oracle::occi::Statement;   ///< Oracle statement type
    using ResultSet = oracle::occi::ResultSet;   ///< Oracle result set type
    using MetaData = oracle::occi::MetaData;     ///< Oracle result set metdata type

    /**
     * Constructor
     * @param connection Connection*, Oracle connection
     * @param sql std::string, SQL statement
     */
    OracleStatement(OracleConnection* connection, const std::string& sql);

    /**
     * Deleted copy constructor
     */
    OracleStatement(const OracleStatement&) = delete;

    /**
     * Move constructor
     */
    OracleStatement(OracleStatement&&) = default;

    /**
     * Destructor
     */
    ~OracleStatement() override;

    /**
     * Deleted copy assignment
     */
    OracleStatement& operator=(const OracleStatement&) = delete;

    /**
     * Move assignment
     */
    OracleStatement& operator=(OracleStatement&&) = default;

    /**
     * Sets actual parameter values for the statement execution
     */
    void setParameterValues() override;

    /**
     * Executes statement
     * @param inTransaction bool, True if statement is executed from transaction
     */
    void execute(bool inTransaction) override;

    /**
     * Executes statement in bulk mode
     * @param inTransaction bool, True if statement is executed from transaction
     * @param lastIteration bool, True if bulk operation is completed (all iterations added)
     */
    void execBulk(bool inTransaction, bool lastIteration);

    /**
     * Closes statement and releases allocated resources
     */
    void close() override;

    /**
     * Fetches next record
     */
    void fetch() override
    {
        if (m_resultSet)
        {
            state().eof = (m_resultSet->next() == ResultSet::END_OF_FETCH);
        }
    }

    /**
     * Returns result set (if returned by a statement)
     */
    ResultSet* resultSet()
    {
        return m_resultSet;
    }

    void getOutputParameters(FieldList& fields);

private:
    Statement* m_createClobStatement {nullptr}; ///< Statement for creating CLOBs
    Statement* m_createBlobStatement {nullptr}; ///< Statement for creating BLOBs
    ResultSet* m_resultSet {nullptr};           ///< Result set (if returned by statement)

    /*
     * Index of output parameters
     */
    std::vector<unsigned> m_outputParamIndex;

    /**
     * Sets character data to a CLOB parameter
     * @param parameterIndex uint32_t, Parameter index
     * @param data unsigned char*, Character data buffer
     * @param dataSize uint32_t, Character data size
     */
    void setClobParameter(uint32_t parameterIndex, unsigned char* data, uint32_t dataSize);

    /**
     * Sets binary data to a BLOB parameter
     * @param parameterIndex uint32_t, Parameter index
     * @param data unsigned char*, Binary data buffer
     * @param dataSize uint32_t, Binary data size
     */
    void setBlobParameter(uint32_t parameterIndex, unsigned char* data, uint32_t dataSize);

    /**
     * Read BLOB field
     * @param index             Column number
     * @param field             Field
     */
    void getBLOBOutputParameter(unsigned int index, const SDatabaseField& field) const;

    /**
     * Read CLOB field
     * @param index             Column number
     * @param field             Field
     */
    void getCLOBOutputParameter(unsigned int index, const SDatabaseField& field) const;

    /**
     * Set CLOB parameter value
     * @param parameterIndex    Parameter number
     * @param parameter         Query parameter
     */
    void setCLOBParameterValue(unsigned int parameterIndex, QueryParameter& parameter);

    /**
     * Set BLOB parameter value
     * @param parameterIndex    Parameter number
     * @param parameter         Query parameter
     */
    void setBLOBParameterValue(unsigned int parameterIndex, QueryParameter& parameter);

    /**
     * Set Date parameter value
     * @param parameterIndex    Parameter number
     * @param parameter         Query parameter
     */
    void setDateParameterValue(unsigned int parameterIndex, const QueryParameter& parameter);

    /**
     * Set DateTime parameter value
     * @param parameterIndex    Parameter number
     * @param parameter         Query parameter
     */
    void setDateTimeParameterValue(unsigned int parameterIndex, const QueryParameter& parameter);

    /**
     * Set int64 parameter value
     * @param parameterIndex    Parameter number
     * @param parameter         Query parameter
     */
    void setInt64ParamValue(unsigned int parameterIndex, const QueryParameter& parameter);

    /**
     * Set bool parameter value
     * @param parameterIndex    Parameter number
     * @param parameter         Query parameter
     */
    void setBooleanParamValue(unsigned int parameterIndex, const QueryParameter& parameter);

    /**
     * Set string parameter value
     * @param parameterIndex    Parameter number
     * @param parameter         Query parameter
     */
    void setStringParamValue(unsigned int parameterIndex, const QueryParameter& parameter);

    /**
     * Set float parameter value
     * @param parameterIndex    Parameter number
     * @param parameter         Query parameter
     */
    void setFloatParamValue(unsigned int parameterIndex, const QueryParameter& parameter);

    /**
     * Set int parameter value
     * @param parameterIndex    Parameter number
     * @param parameter         Query parameter
     */
    void setIntParamValue(unsigned int parameterIndex, const QueryParameter& parameter);

    void getDateOutputParameter(unsigned int index, const SDatabaseField& field) const;

    void getDateTimeOutputParameter(unsigned int index, const SDatabaseField& field) const;
};

} // namespace sptk
