/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                        SIMPLY POWERFUL TOOLKIT (SPTK)                        ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#pragma once

#include <sptk5/CaseInsensitiveCompare.h>
#include <sptk5/db/AutoDatabaseConnection.h>
#include <sptk5/db/DatabaseConnectionString.h>
#include <sptk5/db/PoolDatabaseConnection.h>
#include <sptk5/threads/SynchronizedList.h>
#include <sptk5/threads/SynchronizedQueue.h>

namespace sptk {

/**
 * @addtogroup Database Database Support
 * @{
 */

/**
 * Create driver instance function type
 */
using CreateDriverInstance = PoolDatabaseConnection*(const char* connectString, size_t connectTimeoutSeconds);

/**
 * Destroy driver instance function type
 */
using DestroyDriverInstance = void(PoolDatabaseConnection*);

#ifdef WIN32
/**
 * Windows: Driver DLL handle type
 */
using DriverHandle = HMODULE;

#else
/**
 * Unix: Driver shared library handle type
 */
using DriverHandle = uint8_t*;

#endif

/**
 * Information about loaded database driver
 */
struct SP_EXPORT DatabaseDriver {
    /**
     * Driver SO/DLL handle after load
     */
    DriverHandle m_handle;

    /**
     * Function that creates driver instances
     */
    CreateDriverInstance* m_createConnection;

    /**
     * Function that destroys driver instances
     */
    DestroyDriverInstance* m_destroyConnection;
};

/**
 * Database driver loader
 *
 * Loads and initializes SPTK database driver by request.
 * Already loaded drivers are cached.
 */
class SP_EXPORT DatabaseConnectionPool
    : public DatabaseConnectionString
    , public std::mutex
{
    friend class AutoDatabaseConnection;

public:
    /**
     * Constructor
     *
     * Database connection string is the same for all connections,
     * created with this object.
     * @param connectionString  Database connection string
     * @param maxConnections    Maximum number of connections in the pool
     */
    DatabaseConnectionPool(const String& connectionString, unsigned maxConnections = 100, std::chrono::seconds connectionTimeout = std::chrono::seconds(60));

    [[nodiscard]] DatabaseConnection getConnection();

    std::chrono::seconds connectionTimeout() const
    {
        return m_connectionTimeout;
    }

protected:
    /**
     * Loads database driver
     *
     * First successfull driver load places driver into driver cache.
     */
    void load();

    /**
     * Creates database connection
     */
    [[nodiscard]] SPoolDatabaseConnection createConnection();

    /**
     * Returns used database connection back to the pool
     * @param connection        Database that is no longer in use and may be returned to the pool
     */
    void releaseConnection(const SPoolDatabaseConnection& connection);

private:
    /**
     * Database driver
     */
    DatabaseDriver* m_driver {nullptr};

    /**
     * Function that creates driver instances
     */
    CreateDriverInstance* m_createConnection {nullptr};

    /**
     * Function that destroys driver instances
     */
    DestroyDriverInstance* m_destroyConnection {nullptr};

    /**
     * Maximum number of connections in the pool
     */
    size_t m_maxConnections;
    SynchronizedQueue<SPoolDatabaseConnection> m_pool;       ///< Available connections
    SynchronizedList<SPoolDatabaseConnection> m_connections; ///< All connections
    std::chrono::seconds m_connectionTimeout;                ///< Connection timeout
};

/**
 * @}
 */
} // namespace sptk
