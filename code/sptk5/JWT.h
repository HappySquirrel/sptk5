/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
┌──────────────────────────────────────────────────────────────────────────────┐
│   The code in this module is based JWT C Library, developed by Ben Collins.  │
│   Please see http://github.com/benmcollins/libjwt for more information.      │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#pragma once

#include <sptk5/cutils>
#include <sptk5/xdoc/Document.h>
#include <sstream>

namespace sptk {

/**
 * Java Web Token encoding and decoding
 */
class SP_EXPORT JWT
{
public:
    /** JWT algorithm types. */
    enum class Algorithm : uint8_t
    {
        NONE = 0,
        HS256,
        HS384,
        HS512,
        RS256,
        RS384,
        RS512,
        ES256,
        ES384,
        ES512,
        INVAL
    };

    Algorithm alg {Algorithm::NONE}; ///< Signature encryption algorithm
    String key;                      ///< Signature encryption key
    xdoc::Document grants;           ///< Token content

    /**
         * Constructor
         */
    JWT() = default;

    /**
         * Get JSON element in JSON object element by name.
         * If element doesn't exist in JSON object yet, it's created as JSON null element.
         * If this element is not JSON object, an exception is thrown.
         * @param name              Name of the element in the object element
         * @returns Element for the name, or NULL if not found
         */
    const Variant& get(const String& name) const
    {
        return grants.root()->findOrCreate(name)->getValue();
    }

    /**
         * Set JSON element in JSON object element by name.
         * If element doesn't exist in JSON object yet, it's created as JSON null element.
         * If this element is not JSON object, an exception is thrown.
         * @param name              Name of the element in the object element
         * @returns Element for the name, or NULL if not found
         */
    void set(const String& name, const Variant& data) const
    {
        grants.root()->set(name, data);
    }

    /*
         * Get signature encryption algorithm
         * @return signature encryption algorithm
         */
    Algorithm get_alg() const;

    /**
         * Set signature encryption algorithm
         * @param _alg               Signature encryption algorithm
         * @param _key               Signature encryption key
         */
    void set_alg(Algorithm _alg, const String& _key);

    /**
         * Get signature encryption algorithm name
         * @param _alg               Signature encryption algorithm
         * @return
         */
    static const char* alg_str(Algorithm _alg);

    /**
         * Get signature encryption algorithm from name
         * @param alg               Signature encryption algorithm name
         * @return
         */
    static Algorithm str_alg(const char* alg);

    /**
         * Sign token
         * @param token             Output token data
         * @param str               Data to sign
         */
    void sign(Buffer& token, const char* str) const;

    /**
         * Encode token to stream
         * @param out               Output stream
         */
    void encode(std::ostream& out) const;

    /**
         * Decode token
         * @param token             Input token data
         * @param _key               Optional signature encryption key
         */
    void decode(const char* token, const String& _key = "");

    /**
         * Export token to stream
         * @param output            Output stream
         * @param pretty            If true, produce formatted output
         */
    void exportTo(std::ostream& output, bool pretty) const;

    /**
         * Find string grant value by name
         * @param node                Parent JSON element
         * @param key               Grant name
         * @param found             Optional (output) flag, true is found
         * @return grant value
         */
    static String get_js_string(const xdoc::SNode& node, const String& key, bool* found = nullptr);

    /**
         * Find integer grant value by name
         * @param node                Parent JSON element
         * @param key               Grant name
         * @param found             Optional (output) flag, true is found
         * @return grant value
         */
    static long get_js_int(const xdoc::SNode& node, const String& key, bool* found = nullptr);

    /**
         * Find boolean grant value by name
         * @param node                Parent JSON element
         * @param key               Grant name
         * @param found             Optional (output) flag, true is found
         * @return grant value
         */
    static bool get_js_bool(const xdoc::SNode& node, const String& key, bool* found = nullptr);

    /**
         * Write token head to output stream
         * @param output            Output stream
         * @param pretty            If true then produce formatted output
         */
    void write_head(std::ostream& output, bool pretty) const;

    /**
         * Write token body to output stream
         * @param output            Output stream
         * @param pretty            If true then produce formatted output
         */
    void write_body(std::ostream& output, bool pretty) const;

    /**
         * Verify token
         * @param head              Token head
         * @param sig               Signature
         */
    void verify(const Buffer& head, const Buffer& sig) const;

    /**
         * Sign using SHA algorithm to HMAC format
         * @param out               Output data
         * @param str               Input data
         */
    void sign_sha_hmac(Buffer& out, const char* str) const;

    /**
         * Verify using SHA algorithm in HMAC format
         * @param head              Token head
         * @param sig               Signature
         */
    void verify_sha_hmac(const char* head, const char* sig) const;

    /**
         * Sign using SHA algorithm to PEM format
         * @param out               Output data
         * @param str               Input data
         */
    void sign_sha_pem(Buffer& out, const char* str) const;

    /**
         * Verify using SHA algorithm in PEM format
         * @param head              Token head
         * @param sig_b64           Signature
         */
    void verify_sha_pem(const char* head, const char* sig_b64) const;

private:
    /**
         * Find grant in token content
         * @param node                Parent JSON element
         * @param key               Grant name
         * @return JSON element, or nullptr if not found
         */
    static xdoc::SNode find_grant(const xdoc::SNode& node, const String& key);
};

/**
 * Encode token into Base64 URI format
 * @param buffer                Token data (input and output)
 */
void jwt_base64uri_encode(Buffer& buffer);

/**
 * Decode token from Base64 URI format
 * @param destination           Token data (output)
 * @param src                   Token data (input)
 */
void jwt_b64_decode(Buffer& destination, const char* src);

} // namespace sptk
