/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#include <sptk5/sptk.h>

#include <map>
#include <sptk5/cgui>

#include "CGtkThemeLoader.h"
#include "ThemeUtils.h"

#ifdef _WIN32
#include <windows.h>
#include <winsock2.h>
#endif

using namespace std;
using namespace sptk;

void CThemes::loadGtkButton(const xdoc::SNode& imageNode, std::map<CThemeImageState, std::string>& buttonFileNames)
{
    static const Strings buttonStates("NORMAL|ACTIVE|DFRAME|PRELIGHT", "|"); /// DFRAME is a stub

    bool defaultFrame = (String) imageNode->attributes().get("detail", "") == "buttondefault";

    String fileName = (String) imageNode->attributes().get("file");
    if (fileName.empty())
    {
        fileName = (String) imageNode->attributes().get("overlay_file");
    }

    String state = upperCase((String) imageNode->attributes().get("state", "NORMAL"));
    //string border = imageNode->attributes().get("border");
    String shadow = upperCase((String) imageNode->attributes().get("shadow", "OUT"));
    if (shadow == "ETCHED_IN")
    {
        return;
    }
    if (fileName[0] == '/')
    {
        fileName = fileName.substr(1, 255);
    }
    int buttonState = buttonStates.indexOf(state);
    if (defaultFrame)
    {
        buttonState = THM_DEFAULT_FRAME;
    }
    else if (buttonState > -1 && shadow == "IN")
    {
        buttonState++;
    }
    if (buttonState > -1 && fileName.find(".png") != STRING_NPOS)
    {
        buttonFileNames[CThemeImageState(buttonState)] = m_themeFolder + fileName;
    }
}

void CThemes::loadGtkButtonFileNames(
    xdoc::Document& xml, string XPath, std::map<CThemeImageState, std::string>& buttonFileNames, string orientation)
{
    buttonFileNames.clear();
    auto buttonImages = xml.root()->select(XPath);
    for (auto imageNode: buttonImages)
    {
        if (!orientation.empty() && (String) imageNode->attributes().get("arrow_direction") != orientation)
        {
            continue;
        }
        loadGtkButton(imageNode, buttonFileNames);
    }

    if (buttonFileNames[THM_IMAGE_ACTIVE].empty())
    {
        buttonFileNames[THM_IMAGE_ACTIVE] = buttonFileNames[THM_IMAGE_NORMAL];
    }

    if (buttonFileNames[THM_IMAGE_NORMAL_HIGHLITED].empty())
    {
        buttonFileNames[THM_IMAGE_NORMAL_HIGHLITED] = buttonFileNames[THM_IMAGE_NORMAL];
    }

    if (buttonFileNames[THM_IMAGE_ACTIVE_HIGHLITED].empty())
    {
        buttonFileNames[THM_IMAGE_ACTIVE_HIGHLITED] = buttonFileNames[THM_IMAGE_ACTIVE];
    }
}

void CThemes::loadGtkButtons(xdoc::Document& xml, const String& styleName, CThemeImageCollection& buttons,
                             const String& function)
{
    string XPath("/styles/style[@name='" + styleName + "']/engine[@name='pixmap']/image");
    buttons.loadFromGtkTheme(xml, XPath, "function", function);
}

void CThemes::loadGtkTheme(const String& gtkThemeName)
{
    CGtkThemeParser gtkThemeLoader;
    const String& testThemeName(gtkThemeName);

    try
    {
        gtkThemeLoader.load(testThemeName);
    }
    catch (...)
    {
        return;
    }

    m_gtkTheme = true;

    m_themeFolder = gtkThemeLoader.themeFolder();

    Buffer buffer;
    gtkThemeLoader.xml().exportTo(xdoc::DataFormat::XML, buffer, true);

    try
    { // Debug
        buffer.saveToFile("/svn/sptk5/trunk/" + testThemeName + ".xml");
    }
    catch (...)
    {
    }

    xdoc::Document& xml = gtkThemeLoader.xml();

    /// Load theme colors
    m_colors.loadFromGtkTheme(xml);

    loadGtkButtons(xml, "button", m_normalButtons, "BOX");
    loadGtkButtons(xml, "checkbutton", m_checkButtons, "CHECK");
    loadGtkButtons(xml, "radiobutton", m_radioButtons, "OPTION");
    loadGtkButtons(xml, "combobutton", m_comboButtons, "BOX");

    loadGtkScrollbars(xml);

    m_progressBar[0].loadFromGtkTheme(xml,
                                      "/styles/style[@name='progressbar']/engine[@name='pixmap']/image[@detail='trough']",
                                      "orientation", "HORIZONTAL");
    m_progressBar[1].loadFromGtkTheme(xml,
                                      "/styles/style[@name='progressbar']/engine[@name='pixmap']/image[@detail='bar']",
                                      "orientation", "HORIZONTAL");

    auto bgImageNodes = xml.root()->select("/styles/style/bg_pixmap");
    if (!bgImageNodes.empty())
    {
        String fileName = CThemeImageCollection::gtkFullFileName((String) bgImageNodes[0]->attributes().get("NORMAL"));
        m_background[3] = loadValidatePNGImage(fileName, true);
    }
}
