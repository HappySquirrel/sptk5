/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#include <cstring>
#include <sptk5/Exception.h>
#include <sptk5/net/SSLContext.h>

// This #include statement must be after SSLContext.h, or it breaks Windows compilation
#include <openssl/err.h>

using namespace std;
using namespace sptk;

int SSLContext::s_server_session_id_context = 1;

void SSLContext::throwError(const String& humanDescription)
{
    const unsigned long error = ERR_get_error();
    const auto*         reason = ERR_reason_error_string(error);
    string              errorStr = ERR_error_string(error, nullptr);
    if (reason != nullptr)
    {
        errorStr += string("(): ") + reason;
    }
    throw Exception(humanDescription + "\n" + errorStr);
}

SSLContext::SSLContext(const String& cipherList, bool tlsOnly)
{
    m_ctx = shared_ptr<SSL_CTX>(SSL_CTX_new(SSLv23_method()),
                                [this](SSL_CTX* context)
                                {
                                    const scoped_lock lock(*this);
                                    SSL_CTX_free(context);
                                });
    if (!cipherList.empty())
    {
        SSL_CTX_set_cipher_list(m_ctx.get(), cipherList.c_str());
    }
    SSL_CTX_set_mode(m_ctx.get(), SSL_MODE_ENABLE_PARTIAL_WRITE);
    SSL_CTX_set_session_id_context(m_ctx.get(), bit_cast<const unsigned char*>(&s_server_session_id_context),
                                   sizeof s_server_session_id_context);
    if (tlsOnly)
    {
        const long flags = SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION;
        SSL_CTX_set_options(m_ctx.get(), flags);
    }
}

SSL_CTX* SSLContext::handle()
{
    const scoped_lock lock(*this);
    return m_ctx.get();
}

int SSLContext::passwordReplyCallback(char* replyBuffer, int replySize, int /*rwflag*/, void* userdata)
{
    snprintf(replyBuffer, static_cast<size_t>(replySize), "%s", bit_cast<char*>(userdata));
    replyBuffer[replySize - 1] = '\0';
    return static_cast<int>(strlen(replyBuffer));
}

void SSLContext::loadKeys(const SSLKeys& keys)
{
    const scoped_lock lock(*this);

    m_password = keys.password();

    // Load keys and certificates
    if (SSL_CTX_use_certificate_chain_file(m_ctx.get(), keys.certificateFileName().c_str()) <= 0)
    {
        throwError("Can't use certificate file " + keys.certificateFileName().string());
    }

    // Define password for auto-answer in callback function
    SSL_CTX_set_default_passwd_cb(m_ctx.get(), passwordReplyCallback);
    SSL_CTX_set_default_passwd_cb_userdata(m_ctx.get(), bit_cast<void*>(m_password.c_str()));
    if (SSL_CTX_use_PrivateKey_file(m_ctx.get(), keys.privateKeyFileName().c_str(), SSL_FILETYPE_PEM) <= 0)
    {
        throwError("Can't use private key file " + keys.privateKeyFileName().string());
    }

    if (SSL_CTX_check_private_key(m_ctx.get()) == 0)
    {
        throwError("Can't check private key file " + keys.privateKeyFileName().string());
    }

    // Load the CAs we trust
    if (!keys.caFileName().empty() && SSL_CTX_load_verify_locations(m_ctx.get(), keys.caFileName().c_str(), nullptr) <= 0)
    {
        throwError("Can't load or verify CA file " + keys.caFileName().string());
    }

    if (SSL_CTX_set_default_verify_paths(m_ctx.get()) <= 0)
    {
        throwError("Can't set default verify paths");
    }

    SSL_CTX_set_verify(m_ctx.get(), keys.verifyMode(), nullptr);
    SSL_CTX_set_verify_depth(m_ctx.get(), keys.verifyDepth());
}
