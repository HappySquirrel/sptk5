/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#include <sptk5/xdoc/XMLDocType.h>

using namespace std;
using namespace sptk;
using namespace xdoc;

void Entity::parse(const String& entityTag)
{
    static const RegularExpression matchEntity(
        R"(^<!ENTITY\s*(?<percent>\%)? (?<name>\S+)(?<type> SYSTEM| PUBLIC \w+)? (?<resource>[\w\._]+|".*"))");

    name = "";
    type = Type::SYSTEM;
    id = "";
    resource = "";

    const auto matches = matchEntity.m(entityTag);
    if (matches)
    {
        const auto percents = matches["percent"].value;
        name = matches["name"].value;
        if (!percents.empty())
        {
            name = "%" + name;
        }

        if (const auto typeAndId = matches["type"].value;
            typeAndId == " SYSTEM")
        {
            type = Type::SYSTEM;
            id = "";
        }
        else if (typeAndId.startsWith(" PUBLIC "))
        {
            const int lengthOfPublicWithSpaces = 8;
            type = Type::PUBLIC;
            id = typeAndId.substr(lengthOfPublicWithSpaces);
        }

        resource = matches["resource"].value;
        if (resource[0] == '"')
        {
            resource = resource.substr(1, resource.length() - 2);
        }
    }
}

XMLDocType::XMLDocType(const char* name, const char* public_id, const char* system_id)
    : m_name(name)
{
    if (public_id != nullptr)
    {
        m_public_id = public_id;
    }
    if (system_id != nullptr)
    {
        m_system_id = system_id;
    }
}

struct entity {
    const char* name;
    int replacement_len;
    const char* replacement;
};

using CEntityMap = map<String, const struct entity*>;

static const vector<struct entity> builtin_ent_xml = {
    {"amp", 1, "&"},
    {"lt", 1, "<"},
    {"gt", 1, ">"},
    {"apos", 1, "'"},
    {"quot", 1, "\""},
    {"euro", 1, "€"},
    {"pound", 1, "£"},
};

const char xml_shortcut[] = "&<>'\"";

class XMLEntityCache
{
    CEntityMap           m_hash;
    map<int, CEntityMap> m_replacementMaps;

public:
    explicit XMLEntityCache(const vector<struct entity>& entities) noexcept
    {
        for (const auto& ent: entities)
        {
            m_hash[ent.name] = &ent;
            m_replacementMaps[ent.replacement_len][ent.replacement] = &ent;
        }
    }

    [[nodiscard]] const struct entity* find(const String& ent) const
    {
        if (const auto itor = m_hash.find(ent);
            itor != m_hash.end())
        {
            return itor->second;
        }
        return nullptr;
    }

    const struct entity* encode(const char* str) const;
};

const struct entity* XMLEntityCache::encode(const char* str) const
{
    auto maps = m_replacementMaps.begin();
    for (; maps != m_replacementMaps.end(); ++maps)
    {
        const int         len = maps->first;
        const String fragment(str, static_cast<size_t>(len));
        const CEntityMap& replacements = maps->second;
        auto itor = replacements.find(fragment);
        if (itor != replacements.end())
        {
            return itor->second;
        }
    }
    return nullptr;
}

static const XMLEntityCache xml_entities(builtin_ent_xml);

void XMLDocType::decodeEntities(const char* str, size_t size, Buffer& ret)
{
    Buffer buffer((const uint8_t*) str, size);
    ret.bytes(0);

    auto* start = (char*) buffer.data();
    auto* ptr = start;
    while (*ptr != static_cast<char>(0))
    {
        char* ent_start = strchr(ptr, '&');
        if (ent_start == nullptr)
        {
            break;
        }

        auto* ent_end = strchr(ent_start + 1, ';');
        if (ent_end != nullptr)
        {
            if (const auto len = static_cast<uint32_t>(ent_start - start);
                len != 0)
            {
                ret.append(start, len);
            }
            ptr = appendDecodedEntity(ret, ent_start, ent_end);
            start = ptr;
        }
        else
        {
            ++ptr;
        }
    }
    ret.append(start, strlen(start));
}

char* XMLDocType::appendDecodedEntity(Buffer& ret, const char* ent_start, char* ent_end)
{
    const char ch = *ent_end;
    *ent_end = 0;
    uint32_t    replacementLength = 0;
    const char* rep = this->getReplacement(ent_start + 1, replacementLength);
    *ent_end = ch;
    if (rep != nullptr)
    {
        ret.append(rep, replacementLength);
    }
    else
    {
        ret.append(ent_start, ent_end - ent_start);
    }
    return ent_end + 1;
}

bool XMLDocType::encodeEntities(const char* str, Buffer& ret)
{
    const auto* table = builtin_ent_xml.data();

    bool replaced = false;

    const char* ptr = str;
    Buffer* src = &m_encodeBuffers[0];
    Buffer* dst = &m_encodeBuffers[1];
    dst->bytes(0);
    for (;;)
    {
        const char* pos = strpbrk(ptr, xml_shortcut);
        if (pos != nullptr)
        {
            const auto index = static_cast<uint32_t>(strchr(xml_shortcut, *pos) - xml_shortcut);
            const entity* ent = table + index;
            if (const auto tailBytes = static_cast<uint32_t>(pos - ptr);
                tailBytes != 0)
            {
                dst->append(ptr, tailBytes);
            }
            dst->append('&');
            dst->append(ent->name);
            dst->append(';');
            replaced = true;
            ptr = pos + 1;
        }
        else
        {
            if (ptr != str)
            {
                dst->append(ptr);
                ptr = dst->c_str();
                Buffer* tmp = dst;
                dst = src;
                src = tmp;
                dst->bytes(0);
            }
            break;
        }
    }

    if (!m_entities.empty())
    {
        auto itor = m_entities.begin();
        for (; itor != m_entities.end(); ++itor)
        {
            const String& val = itor->second;
            const auto len = static_cast<uint32_t>(val.length());
            const char* pos = strstr(ptr, val.c_str());
            while (pos != nullptr)
            {
                dst->append(ptr, static_cast<uint32_t>(pos - ptr));
                dst->append('&');
                dst->append(itor->first);
                dst->append(';');
                replaced = true;
                ptr = pos + len;
                pos = strstr(ptr, val.c_str());
            }
            dst->append(ptr);
            ptr = dst->c_str();
            Buffer* tmp = dst;
            dst = src;
            src = tmp;
            dst->bytes(0);
        }
    }

    if (replaced)
    {
        ret.append(src->data(), src->bytes());
    }
    else
    {
        ret.append(ptr);
    }

    return replaced;
}

const char* XMLDocType::getReplacement(const char* name, uint32_t& replacementLength)
{
    // &#123; style entity..
    if (*name == '#')
    {
        const char* ptr = name;
        ++ptr;

        if (*ptr == 'x' || *ptr == 'X')
        {
            ++ptr;
        }

        if (isdigit(*ptr) != 0)
        {
            constexpr int decimal {10};
            m_replacementBuffer[0] = static_cast<char>(strtol(ptr, nullptr, decimal));
            m_replacementBuffer[1] = '\0';
            replacementLength = 1;
            return m_replacementBuffer.data();
        }
    }

    const char* result {nullptr};

    // Find in built-ins, see entities.h
    if (const struct entity* entity = xml_entities.find(name);
        entity != nullptr)
    {
        replacementLength = static_cast<uint32_t>(entity->replacement_len);
        result = entity->replacement;
    }
    else
    {
        // Find in custom attributes
        const auto itor = m_entities.find(name);
        if (itor != m_entities.end())
        {
            const String& rep = itor->second;
            replacementLength = static_cast<uint32_t>(rep.length());
            result = rep.c_str();
        }
    }

    return result;
}
