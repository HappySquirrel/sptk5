/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#include <iomanip>
#include <sptk5/Field.h>

using namespace std;
using namespace sptk;

Field::Field(const String& name)
    : m_name(name)
    , m_displayName(name)
{
    m_view.width = -1;
    m_view.flags = 4; // FL_ALIGN_LEFT
    m_view.visible = true;
    m_view.precision = 3; // default precision, only affects floating point fields
    dataSize(0);
}

String Field::asString() const
{
    constexpr int maxPrintLength = 64;

    String                          result;
    array<char, maxPrintLength + 1> print_buffer {};
    int                             len;

    if (isNull())
    {
        return result;
    }

    switch (dataType())
    {
        case VariantDataType::VAR_BOOL:
            result = get<bool>() != 0 ? "true" : "false";
            break;

        case VariantDataType::VAR_INT:
        case VariantDataType::VAR_IMAGE_NDX:
            len = snprintf(print_buffer.data(), maxPrintLength, "%i", m_data.get<int32_t>());
            result.assign(print_buffer.data(), len);
            break;

        case VariantDataType::VAR_INT64:
#ifndef _WIN32
            len = snprintf(print_buffer.data(), maxPrintLength, "%li", get<int64_t>());
#else
            len = snprintf(print_buffer.data(), maxPrintLength, "%lli", m_data.get<int64_t>());
#endif
            result.assign(print_buffer.data(), len);
            break;

        case VariantDataType::VAR_FLOAT:
            result = doubleDataToString();
            break;

        case VariantDataType::VAR_MONEY:
            result = moneyDataToString();
            break;

        case VariantDataType::VAR_STRING:
        case VariantDataType::VAR_TEXT:
        case VariantDataType::VAR_BUFFER:
            if (isExternalBuffer())
            {
                result = bit_cast<const char*>(get<const uint8_t*>());
            }
            else if (dataType() == VariantDataType::VAR_STRING)
            {
                result = get<String>();
            }
            else
            {
                const auto& buffer = get<Buffer>();
                result.assign(buffer.c_str(), dataSize());
            }
            break;

        case VariantDataType::VAR_DATE:
            result = DateTime(chrono::microseconds(get<int64_t>())).dateString();
            break;

        case VariantDataType::VAR_DATE_TIME:
            result = epochDataToDateTimeString();
            break;

        case VariantDataType::VAR_IMAGE_PTR:
            len = snprintf(print_buffer.data(), maxPrintLength, "%p", bit_cast<const void*>(get<const uint8_t*>()));
            result.assign(print_buffer.data(), len);
            break;

        default:
            throw Exception("Can't convert field " + fieldName() + " to type String");
    }
    return result;
}

String Field::epochDataToDateTimeString() const
{
    const auto& dateTime(get<DateTime>());
    return dateTime.dateString() + " " + dateTime.timeString(DateTime::PF_TIMEZONE, DateTime::PrintAccuracy::SECONDS);
}

String Field::doubleDataToString() const
{
    stringstream output;
    output << fixed << setprecision(static_cast<int>(m_view.precision)) << get<double>();
    return output.str();
}

void Field::exportTo(const xdoc::SNode& node, bool compactXmlMode, bool detailedInfo, bool nullLargeData) const
{
    constexpr size_t minLargeFieldSize {256};

    if (auto value = asString();
        !value.empty())
    {
        xdoc::SNode element;

        if (nullLargeData && value.length() >= minLargeFieldSize)
        {
            value = "";
        }

        if (dataType() == VariantDataType::VAR_TEXT && !value.empty())
        {
            element = node->pushNode(fieldName(), xdoc::Node::Type::CData);
            element->set(value);
        }
        else
        {
            if (compactXmlMode)
            {
                node->attributes().set(fieldName(), value);
            }
            else
            {
                element = node->pushValue(fieldName(), Variant(value));
            }
        }

        if (detailedInfo)
        {
            element->attributes().set("type", Variant::typeName(dataType()));
            element->attributes().set("size", int2string(static_cast<uint32_t>(dataSize())));
        }
    }
}
