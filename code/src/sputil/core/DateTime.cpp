/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#include <cmath>
#include <cstring>
#include <ctime>
#include <sptk5/cutils>

using namespace std;
using namespace chrono;
using namespace sptk;

namespace sptk {

class SP_EXPORT DateTimeFormat
{
public:
    DateTimeFormat() noexcept;

    static void init() noexcept;

    static char parseDateOrTime(String& format, const String& dateOrTime);
};

} // namespace sptk

static constexpr short lastCenturyYear = 1900;
static constexpr short thisCenturyYear = 2000;
static constexpr short base10 = 10;
static constexpr short monthsInYear = 12;
static constexpr short twelweHours = 12;

static const array<short, monthsInYear> gRegularYear = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
static const array<short, monthsInYear> gLeapYear = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

String  DateTime::_dateFormat;
String  DateTime::_datePartsOrder;
String  DateTime::_fullTimeFormat;
String  DateTime::_shortTimeFormat;
char    DateTime::_dateSeparator;
char    DateTime::_timeSeparator;
Strings DateTime::_weekDayNames;
Strings DateTime::_monthNames;

bool    TimeZone::_time24Mode;
String  TimeZone::_timeZoneName;
minutes TimeZone::_timeZoneOffset;
int     TimeZone::_isDaylightSavingsTime;

constexpr int    minutesInHour = 60;
constexpr int    secondsInMinute = 60;
constexpr int    tzMultiplier = 100;
constexpr double millisecondsInSecond = 1000.0;
constexpr size_t maxDateTimeStringLength = 128;

namespace {
// Returns timezone offset in minutes from formats:
// "Z" - UTC
// "[+-]HH24:MM - TZ offset
int decodeTZOffset(const char* tzOffset)
{
    const char* ptr = tzOffset;
    int         sign = 1;
    switch (*ptr)
    {
        case 'Z':
        case 'z':
            return 0;
        case '+':
            ++ptr;
            break;
        case '-':
            ++ptr;
            sign = -1;
            break;
        default:
            break;
    }

    int minutes = 0;
    int hours;
    if (strlen(ptr) > 2)
    {
        if (const char* ptr1 = strchr(ptr, ':'))
        {
            minutes = string2int(ptr1 + 1);
            hours = string2int(ptr);
        }
        else
        {
            constexpr int hundred {100};
            const auto    hoursAndMinutes = string2int(ptr);
            hours = hoursAndMinutes / hundred;
            minutes = hoursAndMinutes % hundred;
        }
    }
    else
    {
        hours = string2int(ptr);
    }

    return sign * (hours * minutesInHour + minutes);
}
} // namespace

char DateTimeFormat::parseDateOrTime(String& format, const String& dateOrTime)
{
    // find a separator char
    size_t     separatorPos = dateOrTime.find_first_not_of("0123456789 ");
    const char separator = dateOrTime[separatorPos];

    const auto* ptr = dateOrTime.c_str();

    format.clear();

    const char* pattern;
    while (ptr != nullptr)
    {
        switch (string2int(ptr))
        {
            case 10:
                pattern = "19"; // hour (12-hour mode)
                TimeZone::time24Mode(false);
                break;
            case 22:
                pattern = "29"; // hour (24-hour mode)
                TimeZone::time24Mode(true);
                break;
            case 48:
            case 59:
                pattern = "59"; // second
                break;
            case 17:
                pattern = "39"; // day
                DateTime::_datePartsOrder += "D";
                break;
            case 6:
                pattern = "19"; // month
                DateTime::_datePartsOrder += "M";
                break;
            case thisCenturyYear:
            case 0:
                pattern = "2999"; // year
                DateTime::_datePartsOrder += "Y";
                break;
            default:
                pattern = nullptr;
                break;
        }
        if (pattern != nullptr)
        {
            format += pattern;
            format += separator;
        }
        if (separatorPos == string::npos)
        {
            break;
        }
        ptr = dateOrTime.c_str() + separatorPos + 1;
        separatorPos = dateOrTime.find(separator, separatorPos + 1);
    }
    format.resize(format.length() - 1);

    return separator;
}

DateTimeFormat::DateTimeFormat() noexcept
{
    init();
}

void DateTimeFormat::init() noexcept
{
    // make a special date and time - today :)
    struct tm atime = {};
    atime.tm_year = 100; // since 1900, -> 2000
    atime.tm_mon = 5;    // June (January=0)
    atime.tm_mday = 17;
    atime.tm_hour = 22;
    atime.tm_min = 48;
    atime.tm_sec = 59;
    atime.tm_wday = 0; // Sunday

#ifdef __unix__
    // For unknown reason this call of setlocale() under Windows makes
    // calls of sprintf to produce access violations. If you know why please
    // tell me.
    setlocale(LC_TIME, "");
    tzset();
#else
    _tzset();
#endif

    // Build local data and time
    constexpr int             bufferLength {32};
    array<char, bufferLength> dateBuffer = {};
    array<char, bufferLength> timeBuffer = {};
    auto                      len = strftime(timeBuffer.data(), bufferLength - 1, "%X", &atime);
    timeBuffer[len] = 0;
    len = strftime(dateBuffer.data(), bufferLength - 1, "%x", &atime);
    dateBuffer[len] = 0;

    // Build local date and time formats
    DateTime::_datePartsOrder[0] = 0;
    DateTime::_dateSeparator = parseDateOrTime(DateTime::_dateFormat, dateBuffer.data());
    DateTime::time24Mode(timeBuffer[0] == '2');

    // Filling up the week day names, as defined in locale.
    // This date should be Monday:
    atime.tm_year = 103; // since 1900, -> 2003
    atime.tm_mon = 9;
    atime.tm_mday = 21;
    atime.tm_hour = 0;
    atime.tm_min = 0;
    atime.tm_sec = 0;
    DateTime::_weekDayNames.clear();
    constexpr int daysInWeek = 7;
    for (int weekDay = 0; weekDay < daysInWeek; ++weekDay)
    {
        atime.tm_wday = weekDay;
        strftime(dateBuffer.data(), bufferLength - 1, "%A", &atime);
        DateTime::_weekDayNames.push_back(dateBuffer.data());
    }

    // Filling up the month names, as defined in locale.
    // This date should be January 1st:
    atime.tm_year = 103; // since 1900, -> 2003
    atime.tm_mon = 1;
    atime.tm_mday = 1;
    atime.tm_hour = 0;
    atime.tm_min = 0;
    atime.tm_sec = 0;
    atime.tm_wday = 3;
    DateTime::_monthNames.clear();
    for (int month = 0; month < monthsInYear; ++month)
    {
        atime.tm_mon = month;
        strftime(dateBuffer.data(), bufferLength - 1, "%B", &atime);
        DateTime::_monthNames.push_back(dateBuffer.data());
    }
#if defined(__BORLANDC__) || _MSC_VER > 1800
    const char* ptr = _tzname[0];
#else
    const char* ptr = tzname[0];
#endif
    len = static_cast<int>(strlen(ptr));

    if (const char* ptr1 = strchr(ptr, ' '); ptr1 != nullptr)
    {
        len = static_cast<int>(ptr1 - ptr);
    }

    TimeZone::timeZoneName(String(ptr, static_cast<unsigned>(len)));

    const time_t              timestamp = time(nullptr);
    array<char, bufferLength> buf {};
    struct tm                 ltime {};
#ifdef _WIN32
    localtime_s(&ltime, &timestamp);
#else
    localtime_r(&timestamp, &ltime);
#endif
    strftime(buf.data(), bufferLength - 1, "%z", &ltime);
    const int  offset = string2int(buf.data());
    const auto offsetMinutes = minutes(offset % tzMultiplier);
    const auto offsetHours = hours(offset / tzMultiplier);
    TimeZone::isDaylightSavingsTime(ltime.tm_isdst == -1 ? 0 : ltime.tm_isdst);
    TimeZone::timeZoneOffset(offsetHours + offsetMinutes);
}

static const DateTimeFormat dateTimeFormatInitializer;

namespace {

void decodeDate(const DateTime::time_point& timePoint, short& year, short& month, short& day, short& dayOfWeek,
                short& dayOfYear,
                bool   gmt)
{
    time_t aTime = DateTime::clock::to_time_t(timePoint);

    tm time = {};
    if (!gmt)
    {
        aTime += static_cast<int>(TimeZone::offset().count()) * secondsInMinute;
    }
    gmtime_r(&aTime, &time);

    year = static_cast<short>(time.tm_year + lastCenturyYear);
    month = static_cast<short>(time.tm_mon + 1);
    day = static_cast<short>(time.tm_mday);
    dayOfWeek = static_cast<short>(time.tm_wday);
    dayOfYear = static_cast<short>(time.tm_yday);
}

void decodeTime(const DateTime::time_point& timePoint, short& hour, short& minute, short& second, short& millisecond, bool gmt)
{
    time_t timestamp = DateTime::clock::to_time_t(timePoint);

    tm time = {};
    if (!gmt)
    {
        timestamp += static_cast<int>(TimeZone::offset().count()) * secondsInMinute;
    }
    gmtime_r(&timestamp, &time);

    hour = static_cast<short>(time.tm_hour);
    minute = static_cast<short>(time.tm_min);
    second = static_cast<short>(time.tm_sec);

    const auto dur = duration_cast<milliseconds>(timePoint.time_since_epoch());
    const auto sec = duration_cast<seconds>(timePoint.time_since_epoch());
    const auto ms = duration_cast<milliseconds>(dur - sec);
    millisecond = static_cast<short>(ms.count());
}

void encodeDate(DateTime::time_point& timePoint, short year, short month, short day)
{
    tm time = {};
    time.tm_year = year - lastCenturyYear;
    time.tm_mon = month - 1;
    time.tm_mday = day;
    time.tm_isdst = TimeZone::isDaylightSavingsTime();

    const time_t aTime = mktime(&time);
    timePoint = DateTime::clock::from_time_t(aTime);
}

short splitDateString(const char* dateString, short* datePart, char& actualDateSeparator)
{
    actualDateSeparator = 0;

    const char* ptr = dateString;
    char*       end = nullptr;
    size_t      partNumber = 0;
    for (; partNumber < 3; ++partNumber)
    {
        errno = 0;
        datePart[partNumber] = static_cast<short>(strtol(ptr, &end, base10));
        if (errno)
        {
            throw Exception("Invalid date string");
        }

        if (*end == static_cast<char>(0))
        {
            break;
        }

        if (actualDateSeparator == static_cast<char>(0))
        {
            actualDateSeparator = *end;
        }
        else if (actualDateSeparator != *end)
        {
            throw Exception("Invalid date string");
        }

        ptr = end + 1;
    }

    return static_cast<short>(partNumber);
}

short splitTimeString(const char* timeString, short* timePart)
{
    static const RegularExpression matchTime(R"(^([0-2]?\d):([0-5]\d):([0-5]\d)(\.\d+)?)");
    const auto                     matches = matchTime.m(timeString);
    if (!matches)
    {
        throw Exception("Invalid time string");
    }

    int partNumber = 0;
    for (; partNumber < 4; ++partNumber)
    {
        const auto& part = matches[partNumber].value;
        if (part.empty())
        {
            break;
        }
        const auto* value = part.c_str();
        if (partNumber == 3)
        {
            ++value;
        } // Skip dot character
        timePart[partNumber] = static_cast<short>(strtol(value, nullptr, base10));
    }

    return static_cast<short>(partNumber);
}

short correctTwoDigitYear(short year)
{
    if (constexpr short yearsOffset = 100;
        year < yearsOffset)
    {
        if (constexpr auto maxShortYearThisCentury = 35;
            year < maxShortYearThisCentury)
        {
            year = static_cast<short>(year + thisCenturyYear);
        }
        else
        {
            year = static_cast<short>(year + lastCenturyYear);
        }
    }
    return year;
}

void encodeTime(DateTime::time_point& timePoint, short hour, short minute, short second, short millisecond)
{
    timePoint += hours(hour) + minutes(minute) + seconds(second) + milliseconds(millisecond);
}

void encodeTime(DateTime::time_point& dt, const char* tim)
{
    bool            afternoon = false;
    array<short, 4> timePart = {0, 0, 0, 0};
    int             tzOffsetMin = 0;

    if (const char* p = strpbrk(tim, "apAPZ+-"); p != nullptr)
    {
        // Looking for AM, PM, or timezone
        while (p != nullptr)
        {
            switch (*p)
            {
                case 'P':
                case 'p':
                    afternoon = true;
                    break;
                case 'A':
                case 'a':
                    break;
                case '+':
                case '-':
                    tzOffsetMin = -decodeTZOffset(p);
                    break;
                default:
                    break;
            }
            p = strpbrk(p + 1, "Z+-");
        }
        tzOffsetMin += static_cast<int>(TimeZone::offset().count());
    }

    if (const short partNumber = splitTimeString(tim, timePart.data());
        partNumber == 0)
    {
        dt = DateTime::time_point();
        return;
    }

    if (afternoon && timePart[0] != twelweHours)
    {
        timePart[0] = static_cast<short>(timePart[0] + twelweHours);
    }

    encodeTime(dt, timePart[0], timePart[1], timePart[2], timePart[3]);
    dt += minutes(tzOffsetMin);
}

void parseDate(const short* datePart, short& month, short& day, short& year);

void encodeDate(DateTime::time_point& timePoint, const char* dat)
{
    array<short, 7> datePart {};

    const char* timePtr = strpbrk(dat, " T");

    char actualDateSeparator = 0;

    if (const short partNumber = splitDateString(dat, datePart.data(), actualDateSeparator);
        partNumber != 0)
    {
        short month = 0;
        short day = 0;
        short year = 0;
        if (actualDateSeparator != DateTime::dateSeparator() && datePart[0] > 31)
        {
            // YYYY-MM-DD format
            year = datePart[0];
            month = datePart[1];
            day = datePart[2];
        }
        else
        {
            parseDate(datePart.data(), month, day, year);
        }

        year = correctTwoDigitYear(year);

        encodeDate(timePoint, year, month, day);
    }
    else
    {
        timePoint = DateTime::time_point();
        timePtr = dat;
    }

    if (timePtr != nullptr)
    { // Time part included into string
        DateTime::time_point timestamp;
        encodeTime(timestamp, timePtr);
        timePoint += timestamp.time_since_epoch();
    }
}

void parseDate(const short* datePart, short& month, short& day, short& year)
{
    const auto datePartsOrder(DateTime::format(DateTime::Format::DATE_PARTS_ORDER, 0));
    for (int ii = 0; ii < 3; ++ii)
    {
        switch (datePartsOrder[ii])
        {
            case 'M':
                month = datePart[ii];
                break;
            case 'D':
                day = datePart[ii];
                break;
            case 'Y':
                year = datePart[ii];
                break;
            default:
                break;
        }
    }
}

int isLeapYear(const int16_t year)
{
    constexpr auto oneHundredYears = 100;
    constexpr auto fourHundredYears = 400;
    return ((year & 3) == 0 && year % oneHundredYears) || ((year % fourHundredYears) == 0);
}

} // namespace

String TimeZone::name()
{
    return _timeZoneName;
}

bool TimeZone::time24Mode()
{
    return _time24Mode;
}

minutes TimeZone::offset()
{
    return _timeZoneOffset;
}

int TimeZone::isDaylightSavingsTime()
{
    return _isDaylightSavingsTime;
}

void TimeZone::set(const String& timeZoneName)
{
#ifdef _WIN32
    _putenv_s("TZ", timeZoneName.c_str());
    _tzset();
#else
    setenv("TZ", timeZoneName.c_str(), 1);
    tzset();
#endif
    dateTimeFormatInitializer.init();
}

void TimeZone::time24Mode(bool mode)
{
    _time24Mode = mode;
}

void TimeZone::timeZoneName(const String& name)
{
    _timeZoneName = name;
}

void TimeZone::timeZoneOffset(std::chrono::minutes offset)
{
    _timeZoneOffset = offset;
}

void TimeZone::isDaylightSavingsTime(int savingsTime)
{
    _isDaylightSavingsTime = savingsTime;
}

void DateTime::time24Mode(bool t24mode)
{
    const char* timeBuffer = "10:48:59AM";

    if (t24mode)
    {
        timeBuffer = "22:48:59";
    }

    TimeZone::time24Mode(t24mode);
    DateTime::_timeSeparator = DateTimeFormat::parseDateOrTime(DateTime::_fullTimeFormat, timeBuffer);
    DateTime::_shortTimeFormat = DateTime::_fullTimeFormat;

    if (const auto pos = DateTime::_fullTimeFormat.find_last_of(DateTime::_timeSeparator); pos != string::npos)
    {
        DateTime::_shortTimeFormat = DateTime::_fullTimeFormat.substr(0, pos);
    }

    if (!TimeZone::time24Mode())
    {
        DateTime::_fullTimeFormat += "TM";
        DateTime::_shortTimeFormat += "TM";
    }
}

//----------------------------------------------------------------
// Constructors
//----------------------------------------------------------------
DateTime::DateTime(short year, short month, short day, short hour, short minute, short second,
                   short millisecond)
{
    try
    {
        encodeDate(m_dateTime, year, month, day);
        encodeTime(m_dateTime, hour, minute, second, millisecond);
    }
    catch (const Exception&)
    {
        m_dateTime = time_point();
    }
}

DateTime::DateTime(const char* dat)
{
    if (dat == nullptr || *dat == static_cast<char>(0))
    {
        return;
    }

    if (*dat == 'n' && strcmp(dat, "now") == 0)
    {
        m_dateTime = clock::now();
        return;
    }

    while (*dat == ' ')
    {
        ++dat;
    }
    if (*dat == static_cast<char>(0))
    {
        m_dateTime = time_point();
        return;
    }

    const Buffer s1(dat);
    char*        s2 = strpbrk(bit_cast<char*>(s1.c_str()), " T");
    if (s2 != nullptr)
    {
        *s2 = 0;
        ++s2;
    }

    try
    {
        if (strchr(s1.c_str(), _dateSeparator) != nullptr || strchr(s1.c_str(), '-') != nullptr)
        {
            encodeDate(m_dateTime, s1.c_str());
            if (s2 != nullptr && strchr(s2, _timeSeparator) != nullptr)
            {
                encodeTime(m_dateTime, s2);
            }
        }
        else
        {
            encodeTime(m_dateTime, s1.c_str());
        }
    }
    catch (const Exception&)
    {
        m_dateTime = time_point();
    }
}

DateTime::DateTime(const time_point& timePoint)
    : m_dateTime(timePoint)
{
}

DateTime::DateTime(const duration& interval)
    : m_dateTime(interval)
{
}

namespace sptk {

int operator<=>(const DateTime& dt1, const DateTime& dt2)
{
    if (dt1.timePoint() < dt2.timePoint())
    {
        return -1;
    }

    if (dt1.timePoint() > dt2.timePoint())
    {
        return 1;
    }

    return 0;
}

bool operator==(const DateTime& dt1, const DateTime& dt2)
{
    return dt1.timePoint() == dt2.timePoint();
}

DateTime operator+(const DateTime& dateTime, const DateTime::duration& duration)
{
    return DateTime(dateTime.timePoint() + duration);
}

DateTime operator-(const DateTime& dateTime, const DateTime::duration& duration)
{
    return DateTime(dateTime.timePoint() - duration);
}

DateTime::duration operator-(const DateTime& dateTime, const DateTime& dt2)
{
    return dateTime.timePoint() - dt2.timePoint();
}


} // namespace sptk

//----------------------------------------------------------------
// Format routine
//----------------------------------------------------------------
void DateTime::formatDate(ostream& str, int printFlags) const
{
    if (zero())
    {
        return;
    }

    time_t timestamp = clock::to_time_t(m_dateTime);

    if ((printFlags & PF_GMT) == 0)
    {
        timestamp += static_cast<int>(TimeZone::offset().count()) * secondsInMinute;
    }

    tm timeStructure {};
    gmtime_r(&timestamp, &timeStructure);

    array<char, maxDateTimeStringLength> buffer {};
    size_t                               len;
    if ((printFlags & PF_RFC_DATE) != 0)
    {
        len = strftime(buffer.data(), sizeof(buffer) - 1, "%F", &timeStructure);
    }
    else
    {
        len = strftime(buffer.data(), sizeof(buffer) - 1, "%x", &timeStructure);
    }
    str << string(buffer.data(), len);
}

void DateTime::formatTime(ostream& str, int printFlags, PrintAccuracy printAccuracy) const
{
    short hour = 0;
    short minute = 0;
    short second = 0;
    short millisecond = 0;

    ::decodeTime(m_dateTime, hour, minute, second, millisecond, (printFlags & PF_GMT) != 0);
    const char* appendix = nullptr;
    bool        amPm = (printFlags & PF_12HOURS) != 0;
    if ((printFlags & PF_TIMEZONE) != 0)
    {
        amPm = false;
    }
    if (amPm)
    {
        if (hour > 11)
        {
            appendix = "PM";
        }
        else
        {
            appendix = "AM";
        }
        if (hour > twelweHours)
        {
            hour = static_cast<short>(hour % twelweHours);
        }
    }

    const char savedFill = str.fill('0');
    str << setw(2) << hour << _timeSeparator << setw(2) << minute;
    switch (printAccuracy)
    {
        case PrintAccuracy::MINUTES:
            break;
        case PrintAccuracy::SECONDS:
            str << _timeSeparator << setw(2) << second;
            break;
        default:
            str << _timeSeparator << setw(2) << second << "." << setw(3) << millisecond;
            break;
    }

    if (amPm)
    {
        str << appendix;
    }

    if ((printFlags & PF_TIMEZONE) != 0)
    {
        if (TimeZone::offset().count() == 0 || (printFlags & PF_GMT) != 0)
        {
            str << "Z";
        }
        else
        {
            minutes offsetMinutes;
            if (TimeZone::offset().count() > 0)
            {
                str << '+';
                offsetMinutes = TimeZone::offset();
            }
            else
            {
                str << '-';
                offsetMinutes = -TimeZone::offset();
            }
            str << setw(2) << offsetMinutes.count() / secondsInMinute << ":" << setw(2) << offsetMinutes.count() % secondsInMinute;
        }
    }

    str.fill(savedFill);
}


void DateTime::decodeDate(short* year, short* month, short* day, short* weekDay, short* yearDate, bool gmt) const
{
    ::decodeDate(m_dateTime, *year, *month, *day, *weekDay, *yearDate, gmt);
}

void DateTime::decodeTime(short* hour, short* minute, short* second, short* millisecond, bool gmt) const
{
    ::decodeTime(m_dateTime, *hour, *minute, *second, *millisecond, gmt);
}


// Get the current system time with optional synchronization offset
DateTime DateTime::Now()
{
    return DateTime(clock::now());
}

short DateTime::daysInMonth() const
{
    short year = 0;
    short month = 0;
    short day = 0;
    short weekDay = 0;
    short yearDay = 0;
    ::decodeDate(m_dateTime, year, month, day, weekDay, yearDay, false);
    return isLeapYear(year) ? gLeapYear[month - 1] : gRegularYear[month - 1];
}

DateTime DateTime::date() const
{
    constexpr int    hoursInDay = 24;
    const duration   sinceEpoch = m_dateTime.time_since_epoch();
    const long       days = duration_cast<hours>(sinceEpoch + seconds(TimeZone::offset())).count() / hoursInDay;
    const time_point timePoint = time_point() + hours(days * hoursInDay);
    return DateTime(timePoint); // Sets the current date
}

short DateTime::dayOfWeek() const
{
    short year = 0;
    short month = 0;
    short day = 0;
    short weekDay = 0;
    short yearDay = 0;

    ::decodeDate(m_dateTime, year, month, day, weekDay, yearDay, false);

    return weekDay;
}

String DateTime::dayOfWeekName() const
{
    return DateTime::_weekDayNames[static_cast<size_t>(dayOfWeek())];
}

String DateTime::monthName() const
{
    short year = 0;
    short month = 0;
    short day = 0;
    short weekDay = 0;
    short yearDay = 0;

    ::decodeDate(m_dateTime, year, month, day, weekDay, yearDay, false);

    return DateTime::_monthNames[static_cast<size_t>(month) - 1];
}

String DateTime::dateString(int printFlags) const
{
    stringstream str;
    formatDate(str, printFlags);
    return str.str();
}

String DateTime::timeString(int printFlags, PrintAccuracy printAccuracy) const
{
    stringstream str;
    formatTime(str, printFlags, printAccuracy);
    return str.str();
}

String DateTime::isoDateTimeString(PrintAccuracy printAccuracy, bool gmt) const
{
    int printFlags = PF_TIMEZONE | PF_RFC_DATE;
    if (gmt)
    {
        printFlags |= PF_GMT;
    }

    return dateString(printFlags) + "T" + timeString(printFlags, printAccuracy);
}

DateTime DateTime::convertCTime(const time_t timestamp)
{
    return DateTime(clock::from_time_t(timestamp));
}

String DateTime::format(Format dtFormat, size_t arg)
{
    switch (dtFormat)
    {
        case Format::DATE_PARTS_ORDER:
            return _datePartsOrder;
        case Format::FULL_TIME_FORMAT:
            return _fullTimeFormat;
        case Format::SHORT_TIME_FORMAT:
            return _shortTimeFormat;
        case Format::MONTH_NAME:
            return _monthNames[arg];
        case Format::WEEKDAY_NAME:
            return _weekDayNames[arg];
        default:
            return _dateFormat;
    }
}

char DateTime::dateSeparator()
{
    return _dateSeparator;
}

double sptk::duration2seconds(const DateTime::duration& duration)
{
    const auto intervalMs = static_cast<double>(chrono::duration_cast<microseconds>(duration).count()) / 1000.0;
    return intervalMs / millisecondsInSecond;
}
