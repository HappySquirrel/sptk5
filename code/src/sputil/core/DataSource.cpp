/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#include <sptk5/DataSource.h>

using namespace std;
using namespace sptk;

bool DataSource::load()
{
    // Loading data into DS
    return loadData();
}

bool DataSource::save()
{
    // Storing data from DS
    return saveData();
}

void DataSource::exportRowTo(const xdoc::SNode& node, bool compactXmlMode, bool nullLargeData)
{
    const auto cnt = fieldCount();
    for (size_t i = 0; i < cnt; ++i)
    {
        const Field& field = operator[](i);
        field.exportTo(node, compactXmlMode, nullLargeData);
    }
}

void DataSource::exportTo(xdoc::Node& parentNode, const String& nodeName, bool compactXmlMode)
{
    try
    {
        open();
        while (!eof())
        {
            const auto& node = parentNode.pushNode(nodeName, xdoc::Node::Type::Object);
            exportRowTo(node, compactXmlMode, false);
            next();
        }
        close();
    }
    catch (const Exception&)
    {
        close();
        throw;
    }
}
