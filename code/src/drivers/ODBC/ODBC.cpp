/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#include <array>
#include <cstring>

#include <sptk5/db/ODBCEnvironment.h>

using namespace std;
using namespace sptk;

namespace {
// Returns true if result code indicates success
inline bool Successful(RETCODE ret)
{
    return ret == SQL_SUCCESS || ret == SQL_SUCCESS_WITH_INFO;
}
} // namespace

//---------------------------------------------------------------------------
// ODBC Environment class
//---------------------------------------------------------------------------

void ODBCEnvironment::allocEnv()
{
    if (valid())
    {
        return;
    } // Already allocated

    const scoped_lock lock(*this);
    SQLHENV hEnvironment {nullptr};
    if (!Successful(SQLAllocEnv(&hEnvironment)))
    {
        throw DatabaseException("Can't allocate ODBC environment");
    }

    m_hEnvironment = shared_ptr<uint8_t>(static_cast<uint8_t*>(hEnvironment),
                                         [this](uint8_t* env) {
                                             const scoped_lock lck(*this);
                                             SQLFreeEnv(env);
                                         });
}

//--------------------------------------------------------------------------------------------
// ODBC Connection class
//--------------------------------------------------------------------------------------------

ODBCEnvironment ODBCConnectionBase::m_env;

void ODBCConnectionBase::allocConnect()
{
    // If already connected, return false
    if (valid())
    {
        return;
    }

    // Allocate environment if not already done
    m_cEnvironment.allocEnv();

    const scoped_lock lock(*this);

    // Create connection handle
    SQLHDBC hConnection = nullptr;
    if (!Successful(SQLAllocConnect(m_cEnvironment.handle(), &hConnection)))
    {
        m_hConnection = SQL_NULL_HDBC;
        throw DatabaseException(errorInformation("Can't alloc connection"));
    }

    m_hConnection = shared_ptr<uint8_t>(static_cast<uint8_t*>(hConnection),
                                        [this](uint8_t* ptr) {
                                            if (m_connected)
                                            {
                                                const SQLHDBC conn(ptr);
                                                SQLDisconnect(conn);
                                                SQLFreeHandle(SQL_HANDLE_DBC, conn);
                                                m_connected = false;
                                                m_connectString = "";
                                            }
                                        });
}

void ODBCConnectionBase::freeConnect()
{
    if (!valid())
    {
        return; // Not connected
    }

    if (isConnected())
    {
        disconnect();
    }

    const scoped_lock lock(*this);

    SQLFreeConnect(m_hConnection.get());
    m_hConnection = SQL_NULL_HDBC;
    m_connected = false;
    m_connectString = "";
}

void ODBCConnectionBase::connect(const String& ConnectionString, String& pFinalString, bool /*EnableDriverPrompt*/)
{
    // Check parameters
    if (ConnectionString.empty())
    {
        throw DatabaseException("Can'connect: connection string is empty");
    }

    // If handle not allocated, allocate it
    allocConnect();

    // If we are  already connected, disconnect
    if (isConnected())
    {
        disconnect();
    }

    const scoped_lock lock(*this);

    m_connectString = ConnectionString;

    constexpr short bufferLength = 2048;
    array<uint8_t, bufferLength> buff {};
    SWORD bufflen = 0;

#ifdef WIN32
    HWND ParentWnd = 0;
#else
    void* ParentWnd = nullptr;
#endif
    char* pConnectString = m_connectString.empty() ? nullptr : &m_connectString[0];
    SQLRETURN rc = ::SQLDriverConnect(m_hConnection.get(), ParentWnd, (UCHAR*) pConnectString, SQL_NTS,
                                      buff.data(), bufferLength, &bufflen, SQL_DRIVER_NOPROMPT);


    if (!Successful(rc))
    {
        const String errorInfo = errorInformation(("SQLDriverConnect(" + ConnectionString + ")").c_str());
        throw DatabaseException(errorInfo);
    }

    pFinalString = (const char*) buff.data();
    m_connected = true;
    m_connectString = pFinalString;

    // Trying to get more information about the driver
    array<uint8_t*, bufferLength> driverDescription {};
    SQLSMALLINT descriptionLength = 0;
    rc = SQLGetInfo(m_hConnection.get(), SQL_DBMS_NAME, driverDescription.data(), bufferLength, &descriptionLength);
    if (Successful(rc))
    {
        m_driverDescription = String((const char*) driverDescription.data());
    }

    rc = SQLGetInfo(m_hConnection.get(), SQL_DBMS_VER, driverDescription.data(), bufferLength, &descriptionLength);
    if (Successful(rc))
    {
        m_driverDescription += " " + String((const char*) driverDescription.data());
    }
}

void ODBCConnectionBase::disconnect()
{
    if (!isConnected())
    {
        return;
    } // Not connected

    const scoped_lock lock(*this);

    SQLDisconnect(m_hConnection.get());
    m_connected = false;
    m_connectString = "";
}

void ODBCConnectionBase::setConnectOption(UWORD fOption, UDWORD vParam)
{
    if (!isConnected())
    {
        throw DatabaseException(errorInformation(cantSetConnectOption));
    }

    const scoped_lock lock(*this);

    if (!Successful(SQLSetConnectOption(m_hConnection.get(), fOption, vParam)))
    {
        throw DatabaseException(errorInformation(cantSetConnectOption));
    }
}

void ODBCConnectionBase::execQuery(const char* query)
{
    SQLHSTMT hstmt = SQL_NULL_HSTMT;

    const scoped_lock lock(*this);

    // Allocate Statement Handle
    if (!Successful(SQLAllocHandle(SQL_HANDLE_STMT, m_hConnection.get(), &hstmt)))
    {
        throw Exception("Can't allocate handle");
    }

    if (Buffer queryBuffer((const uint8_t*) query, strlen(query));
        !Successful(SQLExecDirect(hstmt, queryBuffer.data(), static_cast<SQLINTEGER>(queryBuffer.size()))))
    {
        SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
        throw Exception("Can't execute query: " + String(query));
    }

    SQLFreeHandle(SQL_HANDLE_STMT, hstmt);
}

void ODBCConnectionBase::beginTransaction()
{
    execQuery("BEGIN TRANSACTION");
}

void ODBCConnectionBase::transact(UWORD fType)
{
    if (!isConnected())
    {
        throw DatabaseException(string(cantEndTranscation) + "Not connected to the database");
    }

    if (fType == SQL_COMMIT)
    {
        execQuery("COMMIT");
    }
    else
    {
        execQuery("ROLLBACK");
    }
}

//==============================================================================
String sptk::removeDriverIdentification(const char* error)
{
    if (error == nullptr)
    {
        return "";
    }

    const auto* p = error;
    const char* p1 = error;
    while (p1 != nullptr)
    {
        p1 = strstr(p, "][");
        if (p1 != nullptr)
        {
            p = p1 + 1;
        }
    }
    p1 = strstr(p, "]");
    if (p1 != nullptr)
    {
        p = p1 + 1;
    }

    auto len = static_cast<int>(strlen(p));

    p1 = strstr(p, "sqlerrm(");
    if (p1 != nullptr)
    {
        p = p1 + 8;
        len = static_cast<int>(strlen(p));
        if (p[len - 1] == ')')
        {
            --len;
        }
    }

    return {p, static_cast<size_t>(len)};
}

string extract_error(
    SQLHANDLE handle,
    SQLSMALLINT type)
{
    SQLSMALLINT i = 0;
    SQLINTEGER native = 0;
    array<SQLCHAR, 7> state {};
    array<SQLCHAR, 256> text {};
    SQLSMALLINT len = 0;

    string error;
    for (;;)
    {
        ++i;
        if (SQLGetDiagRec(type, handle, i, state.data(), &native, text.data(), sizeof(text), &len) != SQL_SUCCESS)
        {
            break;
        }
        error += removeDriverIdentification((char*) text.data()) + string(". ");
    }

    return error;
}

String ODBCConnectionBase::errorInformation(const char* function) const
{
    array<char, SQL_MAX_MESSAGE_LENGTH> errorDescription {};
    array<char, SQL_MAX_MESSAGE_LENGTH> errorState {};
    SWORD pcnmsg = 0;
    SQLINTEGER nativeError = 0;

    int rc = SQLError(SQL_NULL_HENV, m_hConnection.get(), SQL_NULL_HSTMT, (UCHAR*) errorState.data(), &nativeError,
                      (UCHAR*) errorDescription.data(), sizeof(errorDescription), &pcnmsg);
    if (rc == SQL_SUCCESS)
    {
        return extract_error(m_hConnection.get(), SQL_HANDLE_DBC);
    }

    rc = SQLError(m_cEnvironment.handle(), SQL_NULL_HDBC, SQL_NULL_HSTMT, (UCHAR*) errorState.data(),
                  &nativeError, (UCHAR*) errorDescription.data(), sizeof(errorDescription), &pcnmsg);
    if (rc != SQL_SUCCESS)
    {
        throw DatabaseException(cantGetInformation);
    }

    return String(function) + ": " + removeDriverIdentification(errorDescription.data());
}
