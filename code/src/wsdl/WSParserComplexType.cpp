/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#include <iomanip>
#include <sptk5/RegularExpression.h>
#include <sptk5/wsdl/WSParserComplexType.h>
#include <sptk5/wsdl/WSTypeTranslator.h>
#include <utility>

using namespace std;
using namespace sptk;

std::map<String, xdoc::SNode> WSParserComplexType::SimpleTypeElements;

WSParserAttribute::WSParserAttribute(String name, const String& typeName)
    : m_name(std::move(name))
    , m_wsTypeName(typeName)
{
    m_cxxTypeName = WSTypeTranslator::toCxxType(typeName);
}

String WSParserAttribute::generate(bool initialize) const
{
    constexpr int fieldNameWidth = 40;
    stringstream  str;
    str << left << setw(fieldNameWidth) << "sptk::WSString"
        << " m_" << m_name;
    if (initialize)
    {
        str << " {\"" << m_name << "\", true}";
    }
    return str.str();
}

WSParserComplexType::WSParserComplexType(const xdoc::SNode& complexTypeElement, const String& name,
                                         const String& typeName)
    : m_name(name.empty() ? complexTypeElement->attributes().get("name") : name)
    , m_typeName(typeName.empty() ? complexTypeElement->attributes().get("type") : typeName)
    , m_element(complexTypeElement)
{
    xdoc::SNode simpleTypeElement = nullptr;
    if (!m_typeName.empty())
    {
        simpleTypeElement = findSimpleType(m_typeName);
    }
    else if (complexTypeElement->getQualifiedName() == "xsd:element")
    {
        simpleTypeElement = m_element;
    }

    if (simpleTypeElement)
    {
        const auto& restrictionElement = simpleTypeElement->findFirst("xsd:restriction");
        if (restrictionElement != nullptr)
        {
            m_typeName = restrictionElement->attributes().get("base");
            m_restriction = make_shared<WSRestriction>(m_typeName, restrictionElement->parent());
        }
    }

    if (const auto& documentationElement = complexTypeElement->findFirst("xsd:documentation");
        documentationElement != nullptr)
    {
        m_documentation = documentationElement->getText().trim();
    }

    if (m_typeName.empty())
    {
        m_typeName = m_name;
    }

    String maxOccurs;
    String minOccurs;
    if (complexTypeElement->attributes().have("maxOccurs"))
    {
        maxOccurs = complexTypeElement->attributes().get("maxOccurs");
    }
    if (complexTypeElement->attributes().have("minOccurs"))
    {
        minOccurs = complexTypeElement->attributes().get("minOccurs");
    }

    m_multiplicity = WSMultiplicity::REQUIRED;

    // Relaxed defaults, in case of incomplete or missing multiplicity
    if (minOccurs.empty())
    {
        minOccurs = "1";
    }
    if (maxOccurs.empty())
    {
        maxOccurs = "1";
    }

    if (minOccurs == "0")
    {
        m_multiplicity = maxOccurs == "1" ? WSMultiplicity::ZERO_OR_ONE : WSMultiplicity::ZERO_OR_MORE;
    }
    else if (minOccurs == "1")
    {
        m_multiplicity = maxOccurs == "1" ? WSMultiplicity::REQUIRED : WSMultiplicity::ONE_OR_MORE;
    }
}

String WSParserComplexType::className() const
{
    if (String cxxType = WSTypeTranslator::toCxxType(m_typeName, "");
        !cxxType.empty())
    {
        return cxxType;
    }

    const size_t pos = m_typeName.find(':');
    return "C" + m_typeName.substr(pos + 1);
}

void WSParserComplexType::parseSequence(const xdoc::SNode& sequence)
{
    for (const auto& node: sequence->nodes())
    {
        if (node->getQualifiedName() == "xsd:element")
        {
            m_sequence.push_back(make_shared<WSParserComplexType>(node));
        }
    }
}

void WSParserComplexType::parse()
{
    m_attributes.clear();
    if (m_element == nullptr)
    {
        return;
    }

    for (const auto& node: m_element->nodes())
    {
        if (node->getQualifiedName() == "xsd:attribute")
        {
            auto attrName = node->attributes().get("name");
            m_attributes[attrName] = make_shared<WSParserAttribute>(attrName, node->attributes().get("type"));
        }
        else if (node->getQualifiedName() == "xsd:sequence")
        {
            parseSequence(node);
        }
    }
}

String WSParserComplexType::wsClassName(const String& name)
{
    return name;
}

void WSParserComplexType::printDeclarationIncludes(ostream& classDeclaration, const set<String>& usedClasses)
{
    Strings includeFiles;
    includeFiles.push_back("#include <sptk5/sptk.h>");
    includeFiles.push_back("#include <sptk5/FieldList.h>");
    includeFiles.push_back("#include <sptk5/db/QueryParameterList.h>");
    includeFiles.push_back("#include <sptk5/wsdl/WSBasicTypes.h>");
    includeFiles.push_back("#include <sptk5/wsdl/WSComplexType.h>");
    includeFiles.push_back("#include <sptk5/wsdl/WSRestriction.h>");

    for (const auto& usedClass: usedClasses)
    {
        includeFiles.push_back("#include \"" + usedClass + ".h\"");
    }

    includeFiles.sort();
    classDeclaration << includeFiles.join("\n") << endl
                     << endl;
}

WSParserComplexType::Initializer WSParserComplexType::makeInitializer() const
{
    Initializer initializer;

    for (const auto& complexType: m_sequence)
    {
        initializer.copyCtor.push_back("m_" + complexType->name() + "(other.m_" + complexType->name() + ")");
        initializer.moveCtor.push_back(
            "m_" + complexType->name() + "(std::move(other.m_" + complexType->name() + "))");
        initializer.copyAssign.push_back("m_" + complexType->name() + " = other.m_" + complexType->name());
        initializer.moveAssign.push_back(
            "m_" + complexType->name() + " = std::move(other.m_" + complexType->name() + ")");
    }

    for (const auto& [name, attribute]: m_attributes)
    {
        initializer.copyCtor.push_back("m_" + name + "(other.m_" + name + ")");
        initializer.moveCtor.push_back(
            "m_" + name + "(std::move(other.m_" + name + "))");
        initializer.copyAssign.push_back("m_" + name + " = other.m_" + name);
        initializer.moveAssign.push_back(
            "m_" + name + " = std::move(other.m_" + name + ")");
    }

    return initializer;
}

void WSParserComplexType::generateDefinition(std::ostream& classDeclaration, sptk::Strings& fieldNames,
                                             sptk::Strings& elementNames, sptk::Strings& attributeNames,
                                             const String& serviceNamespace) const
{
    constexpr int fieldNameWidth = 40;
    const String  className = "C" + wsClassName(m_name);

    classDeclaration << "#pragma once" << endl;
    classDeclaration << endl;

    const auto usedClasses = getUsedClasses();

    printDeclarationIncludes(classDeclaration, usedClasses);

    const String tagName = makeTagName(className);

    classDeclaration << "namespace " << serviceNamespace << " {" << endl
                     << endl;

    classDeclaration << "/**" << endl;
    classDeclaration << " * WSDL complex type class " << className << "." << endl;
    classDeclaration << " * Generated by wsdl2cxx SPTK utility." << endl;
    classDeclaration << " */" << endl;

    classDeclaration << "class WS_EXPORT " << className << " : public sptk::WSComplexType" << endl;
    classDeclaration << "{" << endl;

    classDeclaration << "public:" << endl
                     << endl;
    classDeclaration << "    /**" << endl;
    classDeclaration << "     * ID of the class" << endl;
    classDeclaration << "     */" << endl;
    classDeclaration << "    static sptk::String classId()" << endl;
    classDeclaration << "    {" << endl;
    classDeclaration << "        return \"" << className.substr(1) << "\";" << endl;
    classDeclaration << "    }" << endl
                     << endl;

    for (const auto& [name, value]: m_attributes)
    {
        attributeNames.push_back(name);
    }

    Initializer initializer = makeInitializer();
    if (!m_sequence.empty())
    {
        classDeclaration << "   // Elements" << endl;
        for (const auto& complexType: m_sequence)
        {
            appendMemberDocumentation(classDeclaration, complexType);

            String       cxxType = complexType->className();
            const string optional =
                (static_cast<int>(complexType->multiplicity()) & static_cast<int>(WSMultiplicity::ZERO_OR_ONE)) != 0 ? ", true" : ", false";
            if (complexType->isArray())
            {
                cxxType = "sptk::WSArray<" + cxxType + ">";
            }

            fieldNames.push_back(complexType->name());
            elementNames.push_back(complexType->name());

            classDeclaration << "   " << left << setw(fieldNameWidth) << cxxType << " m_" << complexType->name();
            if (complexType->isArray())
            {
                classDeclaration << " {\"" << complexType->name() << "\"}";
            }
            else
            {
                classDeclaration << " {\"" << complexType->name() << "\"" << optional << "}";
            }
            classDeclaration << ";" << endl;
        }
    }

    appendClassAttributes(classDeclaration, fieldNames, initializer);

    classDeclaration << endl;

    classDeclaration << "   /**" << endl;
    classDeclaration << "    * Constructor" << endl;
    classDeclaration << "    * @param elementName        WSDL element name" << endl;
    classDeclaration << "    * @param optional           Is element optional flag" << endl;
    classDeclaration << "    */" << endl;
    classDeclaration << "   explicit " << className << "(const char* elementName=\"" << tagName
                     << "\", bool optional=false);" << endl
                     << endl;

    classDeclaration << "   /**" << endl;
    classDeclaration << "    * Copy constructor" << endl;
    classDeclaration << "    * @param other              Other object" << endl;
    classDeclaration << "    */" << endl;
    classDeclaration << "   " << className << "(const " << className << "& other);" << endl
                     << endl;

    classDeclaration << "   /**" << endl;
    classDeclaration << "    * Move constructor" << endl;
    classDeclaration << "    * @param other              Other object" << endl;
    classDeclaration << "    */" << endl;
    classDeclaration << "   " << className << "(" << className << "&& other) noexcept;" << endl
                     << endl;

    classDeclaration << "   /**" << endl;
    classDeclaration << "    * Destructor" << endl;
    classDeclaration << "    */" << endl;
    classDeclaration << "   ~" << className << "() override = default;" << endl
                     << endl;

    classDeclaration << "   /**" << endl;
    classDeclaration << "    * Copy assignment" << endl;
    classDeclaration << "    * @param other              Other object" << endl;
    classDeclaration << "    */" << endl;
    classDeclaration << "   " << className << "& operator = (const " << className << "& other) = default;" << endl
                     << endl;

    classDeclaration << "   /**" << endl;
    classDeclaration << "    * Move assignment" << endl;
    classDeclaration << "    * @param other              Other object" << endl;
    classDeclaration << "    */" << endl;
    classDeclaration << "   " << className << "& operator = (" << className << "&& other) noexcept = default;" << endl
                     << endl;

    classDeclaration << "   /**" << endl;
    classDeclaration << "    * Get complex type field names." << endl;
    classDeclaration << "    * @param group              Field group: elements, attributes, or both" << endl;
    classDeclaration << "    * @return list of fields as Strings" << endl;
    classDeclaration << "    */" << endl;
    classDeclaration << "   static const sptk::Strings& fieldNames(sptk::WSFieldIndex::Group group);" << endl;

    classDeclaration << endl;

    classDeclaration << "private:" << endl
                     << endl;
    classDeclaration << "   /**" << endl
                     << "    * Check restrictions" << endl
                     << "    * Throws an exception if any restriction is violated." << endl
                     << "    */" << endl
                     << "   void checkRestrictions() const override;" << endl;

    classDeclaration << "};" << endl;
    classDeclaration << endl;
    classDeclaration << "}" << endl;
}

String WSParserComplexType::makeTagName(const String& className)
{
    String                  tagName = lowerCase(className.substr(1));
    const RegularExpression matchWords("([A-Z]+[a-z]+)", "g");
    if (const auto words = matchWords.m(className.substr(1));
        words)
    {
        Strings wordList;
        for (const auto& word: words.groups())
        {
            wordList.push_back(word.value);
        }
        tagName = wordList.join("_").toLowerCase();
    }
    return tagName;
}

void WSParserComplexType::generateSetFieldIndex(ostream& classDeclaration, const Strings& elementNames,
                                                const Strings& attributeNames)
{
    if (!elementNames.empty())
    {
        classDeclaration << "    WSComplexType::setElements(fieldNames(WSFieldIndex::Group::ELEMENTS), {&m_"
                         << elementNames.join(", &m_") << "});" << endl;
    }
    if (!attributeNames.empty())
    {
        classDeclaration << "    WSComplexType::setAttributes(fieldNames(WSFieldIndex::Group::ATTRIBUTES), {&m_"
                         << attributeNames.join(", &m_") << "});" << endl;
    }
}

void WSParserComplexType::appendClassAttributes(ostream& classDeclaration, Strings& fieldNames,
                                                Initializer& initializer) const
{
    if (!m_attributes.empty())
    {
        classDeclaration << "   // Attributes" << endl;
        for (const auto& [name, attr]: m_attributes)
        {
            classDeclaration << "   " << attr->generate(true) << ";" << endl;
            initializer.copyCtor.push_back("m_" + attr->name() + "(other.m_" + attr->name() + ")");
            initializer.moveCtor.push_back("m_" + attr->name() + "(std::move(other.m_" + attr->name() + "))");
            fieldNames.push_back(attr->name());
        }
    }
}

void WSParserComplexType::appendMemberDocumentation(ostream&                    classDeclaration,
                                                    const SWSParserComplexType& complexType)
{
    if (!complexType->m_documentation.empty())
    {
        classDeclaration << endl;
        classDeclaration << "   /**" << endl;

        for (const Strings rows(complexType->m_documentation, "[\n\r]+", Strings::SplitMode::REGEXP);
             const String& row: rows)
        {
            classDeclaration << "    * " << trim(row) << endl;
        }
        classDeclaration << "    */" << endl;
    }
}

set<String> WSParserComplexType::getUsedClasses() const
{
    set<String> usedClasses;
    // determine the list of used classes
    for (const auto& complexType: m_sequence)
    {
        String cxxType = complexType->className();
        if (cxxType[0] == 'C')
        {
            usedClasses.insert(cxxType);
        }
    }
    return usedClasses;
}

void WSParserComplexType::printImplementationIncludes(ostream& classImplementation, const String& className)
{
    classImplementation << "#include \"" << className << ".h\"" << endl;
    classImplementation << "using namespace std;" << endl;
    classImplementation << "using namespace sptk;" << endl;
}

void WSParserComplexType::printImplementationRestrictions(std::ostream& classImplementation, std::ostream& checks) const
{
    Strings     requiredElements;
    std::size_t restrictionIndex = 0;
    for (const auto& complexType: m_sequence)
    {
        if ((static_cast<int>(complexType->multiplicity()) & static_cast<int>(WSMultiplicity::REQUIRED)) != 0)
        {
            requiredElements.push_back(complexType->name());
        }

        if (!complexType->m_typeName.startsWith("xsd:"))
        {
            continue;
        }

        const String restrictionCheck = addOptionalRestriction(classImplementation, complexType, restrictionIndex);
        if (!restrictionCheck.empty())
        {
            if (restrictionIndex == 1)
            {
                checks << "    // Check value restrictions" << endl;
            }
            checks << restrictionCheck << endl;
        }
    }

    if (restrictionIndex > 0)
    {
        classImplementation << endl;
        checks << endl;
    }

    if (!requiredElements.empty())
    {
        checks << "    // Check 'required' restrictions" << endl;
        for (const auto& requiredElement: requiredElements)
        {
            checks << "    m_" << requiredElement << ".throwIfNull(\"" << wsClassName(m_name) << "." << requiredElement
                   << "\");" << endl;
        }
    }
}

void WSParserComplexType::printImplementationCheckRestrictions(ostream&      classImplementation,
                                                               const String& className) const
{
    classImplementation << "void " << className << "::checkRestrictions() const" << endl
                        << "{" << endl;
    stringstream checks;
    printImplementationRestrictions(classImplementation, checks);
    if (checks.str().empty())
    {
        classImplementation << "    // There are no checks for restrictions" << endl;
    }
    else
    {
        classImplementation << checks.str();
    }
    classImplementation << "}" << endl
                        << endl;
}

String WSParserComplexType::addOptionalRestriction(std::ostream&               implementation,
                                                   const SWSParserComplexType& complexType,
                                                   size_t&                     restrictionIndex) const
{
    String restrictionCheck;
    if (complexType->m_restriction != nullptr)
    {
        ++restrictionIndex;
        const String restrictionName = "restriction_" + int2string(restrictionIndex);
        const auto   restrictionCtor = complexType->m_restriction->generateConstructor(restrictionName);
        if (!restrictionCtor.empty())
        {
            if (restrictionIndex == 1)
            {
                implementation << "    // Restrictions" << endl;
            }
            implementation << "    static const " << restrictionCtor << ";" << endl;
            if (complexType->isArray())
            {
                restrictionCheck = "    for (const auto& item: m_" + complexType->name() + ")\n" +
                                   "        " + restrictionName + ".check(\"" + wsClassName(m_name) + "." +
                                   complexType->name() + "\", item.asString());";
            }
            else
            {
                restrictionCheck =
                    "    " + restrictionName + ".check(\"" + wsClassName(m_name) + "." + complexType->name() +
                    "\", m_" + complexType->name() + ".asString());";
            }
        }
    }
    return restrictionCheck;
}

void WSParserComplexType::generateImplementation(std::ostream& classImplementation, const Strings& fieldNames,
                                                 const Strings& elementNames,
                                                 const Strings& attributeNames, const String& serviceNamespace) const
{
    const String className = "C" + wsClassName(m_name);

    printImplementationIncludes(classImplementation, className);

    classImplementation << "using namespace " << serviceNamespace << ";" << endl
                        << endl;

    classImplementation << "const sptk::Strings& " << className << "::fieldNames(WSFieldIndex::Group group)" << endl;
    classImplementation << "{" << endl;
    classImplementation << "    static const Strings _fieldNames { \"" << fieldNames.join("\", \"") << "\" };" << endl;
    classImplementation << "    static const Strings _elementNames { \"" << elementNames.join("\", \"") << "\" };"
                        << endl;
    classImplementation << "    static const Strings _attributeNames { \"" << attributeNames.join("\", \"") << "\" };"
                        << endl
                        << endl;
    classImplementation << "    switch (group) {" << endl;
    classImplementation << "        case WSFieldIndex::Group::ELEMENTS: return _elementNames;" << endl;
    classImplementation << "        case WSFieldIndex::Group::ATTRIBUTES: return _attributeNames;" << endl;
    classImplementation << "        default: break;" << endl;
    classImplementation << "    }" << endl
                        << endl;
    classImplementation << "    return _fieldNames;" << endl;
    classImplementation << "}" << endl
                        << endl;

    printImplementationConstructors(classImplementation, className, elementNames, attributeNames);

    printImplementationCheckRestrictions(classImplementation, className);

    const RegularExpression matchStandardType("^xsd:");
}

void WSParserComplexType::printImplementationConstructors(ostream& classImplementation, const String& className,
                                                          const Strings& elementNames,
                                                          const Strings& attributeNames) const
{
    auto       tagName = makeTagName(className);
    const auto initializer = makeInitializer();

    classImplementation << className << "::" << className << "(const char* elementName, bool optional)" << endl
                        << ": " << initializer.ctor.join(",\n  ") << endl
                        << "{" << endl;
    generateSetFieldIndex(classImplementation, elementNames, attributeNames);
    classImplementation << "}" << endl
                        << endl;

    classImplementation << className << "::" << className << "(const " << className << "& other)" << endl
                        << ": " << initializer.copyCtor.join(",\n  ") << endl
                        << "{" << endl;
    generateSetFieldIndex(classImplementation, elementNames, attributeNames);
    classImplementation << "}" << endl
                        << endl;

    classImplementation << className << "::"
                        << "" << className << "(" << className << "&& other) noexcept" << endl
                        << ": " << initializer.moveCtor.join(",\n  ") << endl
                        << "{" << endl;
    generateSetFieldIndex(classImplementation, elementNames, attributeNames);
    classImplementation << "}" << endl
                        << endl;
}

void WSParserComplexType::generate(ostream& classDeclaration, ostream& classImplementation,
                                   const String& externalHeader, const String& serviceNamespace) const
{
    if (!externalHeader.empty())
    {
        classDeclaration << externalHeader << endl;
        classImplementation << externalHeader << endl;
    }

    Strings fieldNames;
    Strings elementNames;
    Strings attributeNames;
    generateDefinition(classDeclaration, fieldNames, elementNames, attributeNames, serviceNamespace);
    generateImplementation(classImplementation, fieldNames, elementNames, attributeNames, serviceNamespace);
}

xdoc::SNode WSParserComplexType::findSimpleType(const String& typeName)
{
    const auto itor = SimpleTypeElements.find(typeName);
    if (itor == SimpleTypeElements.end())
    {
        return nullptr;
    }
    return itor->second;
}

void WSParserComplexType::ImplementationParts::print(ostream& output) const
{
    if (!declarations.str().empty())
    {
        output << declarations.str() << endl;
    }
    output << body.str();
    if (!checks.str().empty())
    {
        output << endl
               << checks.str();
    }
}
