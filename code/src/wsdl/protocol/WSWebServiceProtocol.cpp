/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#include <sptk5/Brotli.h>
#include <sptk5/wsdl/protocol/WSWebServiceProtocol.h>

#include <utility>

using namespace std;
using namespace sptk;

WSWebServiceProtocol::WSWebServiceProtocol(HttpReader& httpReader, const URL& url, WSServices& services,
                                           Host host, bool allowCORS, bool keepAlive,
                                           bool suppressHttpStatus)
    : BaseWebServiceProtocol(&httpReader.socket(), httpReader.getHttpHeaders(), services, url)
    , m_httpReader(httpReader)
    , m_host(std::move(host))
    , m_allowCORS(allowCORS)
    , m_keepAlive(keepAlive)
    , m_suppressHttpStatus(suppressHttpStatus)
{
}

void WSWebServiceProtocol::generateFault(Buffer& output, HttpResponseStatus& httpStatus, String& contentType,
                                         const HTTPException& e, bool jsonOutput) const
{
    httpStatus.code = e.statusCode();
    httpStatus.description = e.statusText();

    String errorText(e.what());

    // Remove possibly injected scripts
    errorText = errorText.replace("<script.*</script>", "");

    if (jsonOutput)
    {
        contentType = "application/json";

        xdoc::Document error;
        error.root()->set("error", errorText);
        error.root()->set("status_code", static_cast<int>(e.statusCode()));
        error.root()->set("status_text", e.statusText());
        error.exportTo(xdoc::DataFormat::JSON, output, true);
    }
    else
    {
        contentType = "application/xml";

        xdoc::Document error;
        const auto&    xmlEnvelope = error.root()->pushNode("soap:Envelope");
        xmlEnvelope->attributes().set("xmlns:soap", "http://schemas.xmlsoap.org/soap/envelope/");

        const auto& xmlBody = xmlEnvelope->pushNode("soap:Body");
        const auto& faultNode = xmlBody->pushNode("soap:Fault");
        faultNode->set("faultCode", "soap:Client");
        faultNode->set("faultString", e.what());

        error.exportTo(xdoc::DataFormat::XML, output, true);
    }
}

namespace {
void substituteHostname(Buffer& page, const Host& host)
{
    xdoc::Document wsdl;
    wsdl.load(page);
    if (page[0] == '<')
    {
        const auto node = wsdl.root()->findFirst("soap:address");
        if (node == nullptr)
        {
            throw Exception("Can't find <soap:address> in WSDL file");
        }
        auto location = node->attributes().get("location", "");
        if (location.empty())
        {
            throw Exception("Can't find location attribute of <soap:address> in WSDL file");
        }
        stringstream listener;
        listener << "http://" << host.toString() << "/";
        location = location.replace("http://([^\\/]+)/", listener.str());
        node->attributes().set("location", location);
        wsdl.exportTo(xdoc::DataFormat::XML, page, true);
    }
    else
    {
        const auto serversNode = wsdl.root()->findFirst("servers");
        if (serversNode == nullptr)
        {
            throw Exception("Can't find 'servers' in OpenAPI file");
        }
        const auto urlNode = serversNode->findFirst("url");
        if (urlNode == nullptr)
        {
            throw Exception("Can't find 'servers/url' in OpenAPI file");
        }
        stringstream listener;
        listener << "http://" << host.toString() << "/";
        urlNode->set(listener.str());
        wsdl.exportTo(xdoc::DataFormat::JSON, page, true);
    }
}
} // namespace

RequestInfo WSWebServiceProtocol::process()
{
    constexpr int             okResponseCode(200);
    constexpr int             httpErrorResponseCode(400);
    constexpr int             onlyPostResponseCode(405);
    constexpr int             invalidContentResponseCode(406);
    constexpr int             serverErrorResponseCode(500);
    constexpr chrono::seconds thirtySeconds(30);

    HttpResponseStatus httpStatus {okResponseCode, "OK"};
    String             returnWSDL = "";

    if (m_httpReader.getRequestType() != "POST")
    {
        // If request url is /?wsdl or /wsdl - return service definition
        if (m_httpReader.getRequestType() == "GET" && (m_url.params().has("wsdl") || m_url.path() == "/wsdl"))
        {
            returnWSDL = "wsdl";
        }
        else if (m_httpReader.getRequestType() == "GET" && (m_url.params().has("api") || m_url.path() == "/api"))
        {
            returnWSDL = "api";
        }
        else
        {
            throw HTTPException(onlyPostResponseCode, "Only POST method is supported");
        }
    }

    RequestInfo requestInfo;
    String      contentEncoding;
    String      contentType;

    if (returnWSDL == "wsdl")
    {
        // Requested WSDL content
        contentType = m_httpReader.httpHeader("text/xml");
        const auto& service = m_services.get(m_url.location());
        requestInfo.response.content().set(service.wsdl());
        substituteHostname(requestInfo.response.content(), m_host);
        requestInfo.name = "wsdl";
    }
    else if (returnWSDL == "api")
    {
        // Requested WSDL content
        contentType = m_httpReader.httpHeader("text/json");
        const auto& service = m_services.get(m_url.location());
        requestInfo.response.content().set(service.openapi());
        substituteHostname(requestInfo.response.content(), m_host);
        requestInfo.name = "api";
    }
    else
    {
        const Buffer& contentBuffer = m_httpReader.output();
        m_httpReader.readAll(thirtySeconds);

        contentType = m_httpReader.httpHeader("Content-Type");
        const bool urlEncoded = contentType.find("x-www-form-urlencoded") != string::npos;
        bool       requestIsJSON = true;
        if (urlEncoded)
        {
            contentEncoding = "x-www-form-urlencoded";
        }
        else
        {
            contentEncoding = m_httpReader.httpHeader("Content-Encoding");
        }
        requestInfo.request.input(contentBuffer, contentEncoding);

        auto authentication = getAuthentication();

        auto* startOfMessage = requestInfo.request.content().data();
        auto* endOfMessage = startOfMessage + requestInfo.request.content().bytes();

        while (startOfMessage != endOfMessage && *startOfMessage < 33)
        {
            ++startOfMessage;
        }

        xdoc::Document xmlContent;
        xdoc::Document jsonContent;

        if (startOfMessage != endOfMessage)
        {
            *endOfMessage = 0;
            if (*startOfMessage == '<')
            {
                if (!contentType.contains("/xml"))
                {
                    contentType = "application/xml; charset=utf-8";
                }
                requestIsJSON = false;
                // COUT("INPUT:\n" << startOfMessage);
                processXmlContent((const char*) startOfMessage, xmlContent.root());
            }
            else if (*startOfMessage == '{' || *startOfMessage == '[')
            {
                contentType = "application/json; charset=utf-8";
                processJsonContent((const char*) startOfMessage, jsonContent.root(), requestInfo, httpStatus,
                                   contentType);
            }
            else
            {
                generateFault(requestInfo.response.content(), httpStatus, contentType,
                              HTTPException(invalidContentResponseCode, "Expect JSON or XML content"),
                              requestIsJSON);
            }
        }
        else
        {
            // Empty request content
            // Regular request w/o content
            RESTtoSOAP(m_url, "", xmlContent.root());
        }

        if (httpStatus.code < httpErrorResponseCode)
        {
            String contentTypeStr;
            requestInfo.name = processMessage(requestInfo.response.content(), xmlContent.root(), jsonContent.root(),
                                              authentication, requestIsJSON, httpStatus, contentTypeStr);
        }
    }

    Strings clientAcceptEncoding(header("accept-encoding"), "[,\\s]+", Strings::SplitMode::REGEXP);

    // For errors, always return uncompressed content
    if (httpStatus.code >= httpErrorResponseCode)
    {
        clientAcceptEncoding.clear();
    }

    if (httpStatus.code == serverErrorResponseCode)
    {
        clientAcceptEncoding.clear();
    }

    // COUT("RESPONSE:\n" << requestInfo.response.content() << endl);

    const Buffer outputData = requestInfo.response.output(clientAcceptEncoding);
    contentEncoding = requestInfo.response.contentEncoding();

    Buffer response;
    response.append("HTTP/1.1 ");
    if (m_suppressHttpStatus && httpStatus.code >= httpErrorResponseCode)
    {
        response.append("202 Accepted\r\n");
    }
    else
    {
        response.append(to_string(httpStatus.code) + " " + httpStatus.description + "\r\n");
    }
    response.append("Content-Type: " + contentType + "\r\n");
    response.append("Content-Length: " + to_string(outputData.bytes()) + "\r\n");
    if (m_keepAlive)
    {
        response.append("Connection: keep-alive\r\n");
    }
    if (m_allowCORS)
    {
        response.append("Access-Control-Allow-Origin: *\r\n");
    }
    if (!contentEncoding.empty())
    {
        response.append("Content-Encoding: " + contentEncoding + "\r\n");
    }

    // OWASP-suggested headers
    response.append("X-Content-Type-Options: nosniff\r\n");

    response.append("\r\n", 2);
    response.append(outputData);
    socket().write(response);

    return requestInfo;
}

shared_ptr<HttpAuthentication> WSWebServiceProtocol::getAuthentication()
{
    shared_ptr<HttpAuthentication> authentication;

    if (const auto itor = headers().find("authorization");
        itor != headers().end())
    {
        const String value(itor->second);
        authentication = make_shared<HttpAuthentication>(value);
        if (authentication->type() == HttpAuthentication::Type::UNDEFINED)
        {
            authentication.reset();
        }
    }

    return authentication;
}
