/*
╔══════════════════════════════════════════════════════════════════════════════╗
║                       SIMPLY POWERFUL TOOLKIT (SPTK)                         ║
║                       ssl_server_test.cpp - description                      ║
╟──────────────────────────────────────────────────────────────────────────────╢
║  begin                Thursday May 25 2000                                   ║
║  copyright            © 1999-2024 Alexey Parshin. All rights reserved.       ║
║  email                alexeyp@gmail.com                                      ║
╚══════════════════════════════════════════════════════════════════════════════╝
┌──────────────────────────────────────────────────────────────────────────────┐
│   This library is free software; you can redistribute it and/or modify it    │
│   under the terms of the GNU Library General Public License as published by  │
│   the Free Software Foundation; either version 2 of the License, or (at your │
│   option) any later version.                                                 │
│                                                                              │
│   This library is distributed in the hope that it will be useful, but        │
│   WITHOUT ANY WARRANTY; without even the implied warranty of                 │
│   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library   │
│   General Public License for more details.                                   │
│                                                                              │
│   You should have received a copy of the GNU Library General Public License  │
│   along with this library; if not, write to the Free Software Foundation,    │
│   Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.               │
│                                                                              │
│   Please report all bugs and problems to alexeyp@gmail.com.                  │
└──────────────────────────────────────────────────────────────────────────────┘
*/

#include <sptk5/cnet>
#include <sptk5/cutils>

using namespace std;
using namespace sptk;

void readAndReply(SSLSocket& socket)
{
    array<char, 1024> buffer;
    array<char, 1024> reply;
    size_t bytes;

    const char* HTMLecho = "<html><body><pre>%s</pre></body></html>\n\n";

    if (!socket.readyToRead(chrono::seconds(3)))
    {
        CERR("Read timeout");
        return;
    }

    bytes = socket.socketBytes();
    if (bytes == 0)
    {
        COUT("Connection is closed by client");
        return;
    }

    if (bytes > sizeof(buffer))
        bytes = sizeof(buffer);

    bytes = socket.read((uint8_t*) buffer.data(), bytes);
    buffer[bytes] = 0;
    COUT("Client msg: " << buffer.data());
    snprintf(reply.data(), sizeof(reply), HTMLecho, buffer);
    socket.write((const uint8_t*) reply.data(), strlen(reply.data()));
}

void processConnection(SocketType clientSocketFD)
{
    SSLSocket connection;
    const SSLKeys keys("key.pem", "cert.pem", "");
    connection.loadKeys(keys);
    try
    {
        connection.attach(clientSocketFD, false);
        readAndReply(connection); /* service connection */
        connection.close();
    }
    catch (const Exception& e)
    {
        CERR(e.what());
    }
}

int main(int argc, const char* argv[])
{
    const int port = string2int(argv[1]);

    if (argc != 2 || port == 0)
    {
        COUT("Usage: " << argv[0] << "<portnum>");
        return 1;
    }

    try
    {
        TCPSocket server;
        server.host(Host("localhost", 3000));

        server.listen();

        constexpr chrono::milliseconds acceptTimeout {500};

        while (true)
        {
            SocketType clientSocketFD;
            struct sockaddr_in clientInfo = {};

            if (server.accept(clientSocketFD, clientInfo, acceptTimeout))
            {
                COUT("Connection: " << inet_ntoa(clientInfo.sin_addr) << static_cast<unsigned>(ntohs(clientInfo.sin_port)));

                processConnection(clientSocketFD);
            }
        }
    }
    catch (const Exception& e)
    {
        CERR(e.what());
    }
}
